;; The first three lines of this file were inserted by DrRacket. They record metadata
;; about the language level of this file in a form that our tools can easily process.
#reader(lib "htdp-intermediate-lambda-reader.ss" "lang")((modname fsm-3) (read-case-sensitive #t) (teachpacks ()) (htdp-settings #(#t constructor repeating-decimal #f #t none #f () #f)))
; fsm-3.rkt : GUI Tool for building and using Finite State Machines.
(require "extras.rkt")
(require rackunit)
(require 2htdp/image)
(require 2htdp/universe)

(provide make-transition
         transition-label
         transition-dest
         make-machine-state
         machine-state-name
         machine-state-is-start?
         machine-state-is-accepting?
         machine-state-transitions
         machine-state-next
         next-states
         is-machine?
         machine-states-reached-from
         machine-accepts?
         run
         initial-world
         world-after-key-event
         world-after-mouse-event
         world-machine
         world-selected-states
         world-with-selected-states
         world-with-new-state
         world-with-new-transition)

(check-location "07" "fsm-3.rkt")

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Information Analysis:
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;; An InputSymbol is a string of length 1 whose one character is
;;; one of the ten decimal digits
;;; or one of the 26 lower-case letters "a" through "z".

;;; An InputString is a ListOfInputSymbol.
;;;(The InputString represented by the empty list is called epsilon.)

;;; A Machine is a ListOfMachineState for which is-machine? returns true
;;; and represents a finite state machine.


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; CONSTANTS
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(define NEWKEY "N")
(define TRANSKEY "T")
(define ACCKEY "A")
(define RETKEY "\r")
(define SELKEY "S")
(define UNSELKEY "U")
(define DELKEY "D")

(define ASCII-LOWER-A 97)
(define ASCII-LOWER-Z 122)
(define ASCII-ZERO 48)
(define ASCII-NINE 57)

(define COUNT-THREE 2)
(define COUNT-TWO 2)
(define COUNT-ONE 1)
(define COUNT-ZERO 0)
(define COUNT-HUNDRED 100)
(define COUNT-THIRTY 30)
(define MAX-NAME-LEN 6)
(define PALE-BLUE (make-color 207 236 236 100))
(define PALE-GRAY (make-color 190 190 190))
(define NEW-MODE-CANVAS-COLOR PALE-GRAY)
(define TRANSITION-MODE-CANVAS-COLOR PALE-GRAY)
(define NORMAL-MODE-CANVAS-COLOR "white")
(define CANVAS-HEIGHT 500)
(define CANVAS-WIDTH 600)
(define CANVAS (empty-scene CANVAS-WIDTH CANVAS-HEIGHT))

(define OUTER-CIRCLE-RADIUS 20)
(define INNER-CIRCLE-RADIUS 17)
(define STATE-NAME-COLOR "black")
(define START-STATE-COLOR "red")
(define OTHER-STATES-COLOR "black")
(define STATE-DRAWING-MODE "outline")
(define SELECTED-CIRCLE-RADIUS 20)
(define SELECTED-STATE-COLOR PALE-BLUE)
(define SELECTED-STATE-DRAWING-MODE "solid")
(define SEPARATE-CIRCLE-RADIUS 20)
(define SEPARATE-STATE-COLOR "white")
(define SEPARATE-STATE-DRAWING-MODE "solid")
(define STATE-NAME-FONT-SIZE 10)
(define MAX-CHRACTERS-OF-STATE-NAME 6)
(define INPUT-SYMBOL-FONT-SIZE 10)
(define INPUT-SYMBOL-FONT-COLOR "black")
(define INPUT-SYMBOL-X-POS-WEIGHT 0.7)
(define INPUT-SYMBOL-X-POS-OFFSET 35)
(define INPUT-SYMBOL-Y-POS-WEIGHT 0.7)
(define INPUT-SYMBOL-Y-POS-OFFSET -37)

;;; Transition Curves
(define TRANSITION-CURVE-TO-ITSELF-END-PULL 7)
(define TRANSITION-CURVE-TO-ITSELF-START-PULL 7)
(define TRANSITION-CURVE-START-PULL 1/2)
(define TRANSITION-CURVE-END-PULL 1/2)
(define TRANSITION-CURVE-TO-ITSELF-END-ANGLE 180)
(define TRANSITION-CURVE-END-ANGLE-RIGHT-DOWN 15)
(define TRANSITION-CURVE-END-ANGLE-RIGHT-UP -15)
(define TRANSITION-CURVE-END-ANGLE-LEFT-DOWN -90)
(define TRANSITION-CURVE-END-ANGLE-LEFT-UP 90)
(define TRANSITION-CURVE-TO-ITSELF-START-ANGLE 70)
(define TRANSITION-CURVE-START-ANGLE-DOWN -30)
(define TRANSITION-CURVE-START-ANGLE-UP 30)
(define TRANSITION-CURVE-TO-ITSELF-START-X-OFFSET 12)
(define TRANSITION-CURVE-TO-ITSELF-START-Y-OFFSET -16)
(define TRANSITION-CURVE-TO-ITSELF-END-X-OFFSET 16)
(define TRANSITION-CURVE-TO-ITSELF-END-Y-OFFSET -12)
(define TRANSITION-CURVE-COLOR "black")
(define TRANSITION-ARROW-SYMBOL ">")
(define TRANSITION-ARROW-SIZE 20)
(define TRANSITION-ARROW-COLOR "black")

(define MACHINE-START-STATE (circle 20 "outline" "red"))
(define MACHINE-NORMAL-STATE (circle 20 "outline" "black"))
(define MACHINE-SELECTED-STATE (circle 20 "solid" PALE-BLUE))
(define INITIAL-X 50)
(define INITIAL-Y 50)
(define MAX-STATES-IN-ROW 6)
;MouseEvents
(define PRESS "button-down")
(define RELEASE "button-up")
(define DRAG "drag")

(define DEFAULT-POSN (make-posn 0 0))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Data Definition:
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Transition
(define-struct transition [label dest])

;;; A Transition is a
;;; -- (make-transition InputSymbol String)
;;; INTERPRETATION :
;;; A Transition represents an edge leading from a state to another state
;;; label - a machine input.
;;; dest  - The name of a state labelled by the machine input

;;; TEMPLATE :
;;; transition-fn : Transition -> ??
;;; (define (transition-fn t)
;;;  (...
;;;   (transition-label t)
;;;   (transition-dest t)))

;;; EXAMPLES :
(define transition1 (make-transition "A" "STATE1"))
(define transition2 (make-transition "B" "STATE2"))



;;; A ListOfTransition (LOT) is a
;;;  -- empty
;;;  -- cons Transition LOT

;;; lot-fn : LOT -> ??
;;;(define (lot-fn l)
;;;  (cond
;;;    [(empty? l) ...]
;;;    [else (...
;;;           (transition-fn (first l))
;;;           (lot-fn (rest l)))]))

;;; EXAMPLES :
(define transitionlist1 (cons transition1 empty))
(define transitionlist2 (cons transition1 (cons transition2 empty)))
(define transitionlist3 (list transition1 transition2))

;;; TESTS :
(begin-for-test
  (check-equal? (empty? empty) true)
  (check-equal? (empty? (cons transition1 empty)) false)
  (check-equal? (empty? (cons transition2 (cons transition1 empty))) false)
  
  (check-equal? (first (cons transition1 empty)) transition1)
  (check-equal? (rest  (cons transition1 empty)) empty)
  
  (check-equal?
   (first (cons transition2 (cons transition1 empty)))
   transition2)
  
  (check-equal?
   (rest  (cons transition2 (cons transition1 empty)))
   (cons transition1 empty))
  
  (check-equal? transitionlist2 transitionlist3)
  
  (check-error
   (first empty)
   "first: expected argument of type <non-empty list>; given empty")
  
  (check-error
   (rest  empty)
   "rest: expected argument of type <non-empty list>; given empty")
  )


;;; MachineState
(define-struct machine-state [name is-start? is-accepting? transitions])

;;; A MachineState is a
;;; -- (make-machine-state String Boolean Boolean ListOfTransition)

;;; INTERPRETATION :
;;; A MachineState represents one state of a finite state machine.
;;; name - The name of the MachineState.
;;; is-start? telss whether it is a start state.
;;; is-accepting? telss it is an accepting state.
;;; transitions - a list of transitions out of this MachineState.

;;; TEMPLATE :
;;; machine-state-fn : MachineState -> ??
;;; (define (machine-state-fn s)
;;;  (...
;;;   (machine-state-name s)
;;;   (machine-state-is-start? s)
;;;   (machine-state-is-accepting? s)
;;;   (machine-state-transitions s)))

;;; EXAMPLES :
;;;   (make-machine-state "S0" true false empty)
;;;   (machine-state-name (make-machine-state "S0" true false empty))
;;;    => "S0" is the name of the MachineState.
;;;   (machine-state-is-start? (make-machine-state "S0" true false empty))
;;;    => true. This is a Starting State.
;;;   (machine-state-is-accepting? (make-machine-state "S0" true false empty))
;;;    => false. This is not an Accepting State.
;;;   (machine-state-transitions (make-machine-state "S0" true false empty))
;;;    => empty. No transitions are possible.

(define machinestate1 (make-machine-state "S0" true false empty))
(define machinestate2 (make-machine-state "S1" false true empty))

;;; A ListOfMachineState (LOM) is a
;;;  -- empty
;;;  -- cons MachineState LOM

;;; lom-fn : LOM -> ??
;;; (define (lom-fn l)
;;;  (cond
;;;    [(empty? l) ...]
;;;    [else (...
;;;           (machine-state-fn (first l))
;;;           (lom-fn (rest l)))]))


;;; EXAMPLES :
(define machinestatelist1 (cons machinestate1 empty))
(define machinestatelist2 (cons machinestate1 (cons machinestate2 empty)))
(define machinestatelist3 (list machinestate1 machinestate2))

;;; TESTS :
(begin-for-test
  (check-equal? (empty? empty) true)
  (check-equal? (empty? (cons machinestate1 empty)) false)
  (check-equal? (empty? (cons machinestate2 (cons machinestate1 empty))) false)
  
  (check-equal? (first (cons machinestate1 empty)) machinestate1)
  (check-equal? (rest  (cons machinestate1 empty)) empty)
  
  (check-equal?
   (first (cons machinestate2 (cons machinestate1 empty)))
   machinestate2)
  
  (check-equal?
   (rest  (cons machinestate2 (cons machinestate1 empty)))
   (cons machinestate1 empty))
  
  (check-equal? machinestatelist2 machinestatelist3)
  
  (check-error
   (first empty)
   "first: expected argument of type <non-empty list>; given empty")
  
  (check-error
   (rest  empty)
   "rest: expected argument of type <non-empty list>; given empty")
  )

;;; MachineStateGUI
(define-struct machine-state-gui
  [ms x y selected?])

;;; A MachineStateGUI is a
;;; -- (make-machine-state-gui MachineState NonNegInteger NonNegInteger Boolean)

;;; INTERPRETATION :
;;; A MachineStateGUI represents one state of a finite state machine with added
;;; Informations for Graphical representation of the MachineState.
;;; ms - A state (MachineState).
;;; x - X coordinate value of the center of the state.
;;; y - Y coordinate value of the center of the state.
;;; Where :  x and y values range between (0-600) and (0-500) respectively.
;;; selected? - Boolean to denote if the state is selected or not.
;;; TEMPLATE :
;;; machine-state-gui-fn : MachineStateGUI -> ??
;;; (define (machine-state-gui-fn msg)
;;;  (...
;;;   (machine-state-gui-name msg)
;;;   (machine-state-gui-x msg)
;;;   (machine-state-gui-y msg)
;;;   (machine-state-gui-selected? msg)))

;;; EXAMPLES : (Lets consider first MachineState in the World)
;;;   ms = (make-machine-state "S0" true false empty) 
;;;   msg = (make-machine-state-gui ms 50 50 #f)
;;;   (machine-state-gui-ms msg)
;;;    => ms
;;;   (machine-state-gui-x msg) 
;;;    => X coordinate of the MachineState (50)
;;;   (machine-state-gui-y msg)
;;;    => Y coordinate of the MachineState (50)
;;;   (machine-state-gui-selected? msg)
;;;    => false. (not selected.)

(define machinestategui1
  (make-machine-state-gui (make-machine-state "S0" true false empty) 50 50 #t))
(define machinestategui2
  (make-machine-state-gui (make-machine-state "S1" false true empty) 100 50 #f))

(define machinestategui3
  (make-machine-state-gui
   (make-machine-state
    "S0" true false
    (list (make-transition "A" "S0") (make-transition "B" "S1"))) 
   20 20 false))

(define machinestategui4
  (make-machine-state-gui
   (make-machine-state
    "S1" false false
    (list (make-transition "B" "S1") (make-transition "C" "S2"))) 
   40 40 false))

(define machinestategui5
  (make-machine-state-gui
   (make-machine-state "S2" false true (list (make-transition "A" "S0")))
  60 60 false))

(define machinestategui6
  (make-machine-state-gui
   (make-machine-state "S3" false true (list (make-transition "A" "S0")))
  60 20 true))

(define machinestategui7
  (make-machine-state-gui
   (make-machine-state "1234567" false false (list (make-transition "A" "S0")))
  60 20 true))

(define gui1
  (make-machine-state-gui
   (make-machine-state
    "S0" true false
    (list (make-transition "A" "S0") (make-transition "B" "S1"))) 
   20 20 false))

(define gui2
  (make-machine-state-gui
   (make-machine-state
    "S1" false false
    (list (make-transition "B" "S1") (make-transition "C" "S2"))) 
   40 40 false))

(define gui3
  (make-machine-state-gui
   (make-machine-state "S1" false false empty) 
   40 40 false))

(define guilist1 (list gui1 gui3))
(define guilist2 (list gui3))




;;; A ListOfMachineStateGUI (LOMSG) is a
;;;  -- empty
;;;  -- cons MachineStateGUI LOMSG

;;; lomsg-fn : LOMSG -> ??
;;; (define (lomsg-fn l)
;;;  (cond
;;;    [(empty? l) ...]
;;;    [else (...
;;;           (machine-state-gui-fn (first l))
;;;           (lomsg-fn (rest l)))]))

;;; EXAMPLES :
(define machinestateguilist1 (cons machinestategui1 empty))
(define machinestateguilist2
  (cons machinestategui1 (cons machinestategui2 empty)))
(define machinestateguilist3 (list machinestategui1 machinestategui2))
(define machinestateguilist4
  (list machinestategui3 machinestategui4 machinestategui5))


;;; A Mode is a
;;; -- Normal
;;; -- New
;;; -- Trans
;;; -- IS
;;; -- C1
;;; -- C2
(define MODE-NORMAL "Normal")
(define MODE-NEW "New")
(define MODE-TRANS "Trans")
(define MODE-IS "IS")
(define MODE-C1 "C1")
(define MODE-C2 "C2")
;;; Interpretation:
;;; normal - Normal Mode.
;;; N - Key N is pressed and waiting for state name.
;;; T - Key T is pressed and waiting for InputSymbol.
;;; IS - InputSymbol is Given and waiting for Click 1.
;;; C1 - State 1 is pressed and waiting for Click 2.
;;; C2 - State 2 is pressed and a transition is about to be created.
;;; Template
;;;mode-fn : Mode -> ??
;;;(define (mode-fn m)
;;;  (cond
;;;    [(string=? m MODE-NORMAL) ...]
;;;    [(string=? m MODE-NEW) ...]
;;;    [(string=? m MODE-TRANS) ...]
;;;    [(string=? m MODE-IS) ...]
;;;    [(string=? m MODE-C1) ...]
;;;    [(string=? m MODE-C2) ...]))


;;; World
(define-struct world [lomsg mode name srcstate dststate statecount mouseprev])
;;; A World is a
;;; --(make-world
;;;     ListOfMachineStateGUI Mode String MachineState
;;;     Transition NonNegInteger POSN)

;;; Interpretation :
;;; lomsg  - list of machine states with GUI info being displayed in that world.
;;; mode - is the current mode of the World.
;;; name - the new state name being entered. (at MODE-NEW mode) or
;;;        InputSymbol (at MODE-TRANS mode)
;;; srcstate - Source state of the new Transition being constructed.
;;; dststate - Dest state of the new Transition being constructed.
;;; statecount - Number of states created so far.
;;; mouseprev - the previous x, y coordinates of the mouse pointer.

;;; Template:
;;; world-fn : World -> ??
;;; (define (world-fn w)
;;;   (...
;;;    (world-lomsg w)
;;;    (world-mode w)
;;;    (world-name w)
;;;    (world-srcstate w)
;;;    (world-dststate w)
;;;    (world-statecount w)
;;;    (world-mouseprev w)))

;;; Example :
;;; (make-world (list s0 s1) MODE-NORMAL "" '() '() 2)
;;;   => Machine Has Two States s0 and S1.
;;;   => World in Normal mode.
;;;   => name is empty
;;;   => dststate and srcstate are empty
;;;   => So far 2 states have been created.
;;;   => Previous x,y coordinates are 0,0
(define INITIAL-WORLD
  (make-world '()  MODE-NORMAL "" '() '() COUNT-ZERO DEFAULT-POSN))

(define world1
  (make-world
   machinestateguilist4 MODE-NORMAL "" '() '() COUNT-THREE DEFAULT-POSN))

(define world2
  (make-world
   (list machinestategui6) MODE-NORMAL "" '() '() COUNT-ONE DEFAULT-POSN))

(define world3
  (make-world
   guilist2 MODE-NORMAL "" '() '() COUNT-ONE DEFAULT-POSN))
;;; An InputString is a ListOfInputSymbol(LOSYM) is a
;;;  -- empty
;;;  -- cons InputSymbol LOSYM

;;; losym-fn : LOSYM -> ??
;;; (define (losym-fn l)
;;;  (cond
;;;    [(empty? l) ...]
;;;    [else (...
;;;           (... (first l))
;;;           (losym-fn (rest l)))]))

;;; EXAMPLES :
(define inputsymbol1 "a")
(define inputsymbol2 "b")

(define inputstring1 (cons inputsymbol1 empty))
(define inputstring2 (cons inputsymbol1 (cons inputsymbol2 empty)))
(define inputstring3 (list inputsymbol1 inputsymbol2))

;;; TESTS :
(begin-for-test
  (check-equal? (empty? empty) true)
  (check-equal? (empty? (cons inputsymbol1 empty)) false)
  (check-equal? (empty? (cons inputsymbol2 (cons inputsymbol1 empty))) false)
  
  (check-equal? (first (cons inputsymbol1 empty)) inputsymbol1)
  (check-equal? (rest  (cons inputsymbol1 empty)) empty)
  
  (check-equal?
   (first (cons inputsymbol2 (cons inputsymbol1 empty)))
   inputsymbol2)
  
  (check-equal?
   (rest  (cons inputsymbol2 (cons inputsymbol1 empty)))
   (cons inputsymbol1 empty))
  
  (check-equal? inputstring2 inputstring3)
  
  (check-error
   (first empty)
   "first: expected argument of type <non-empty list>; given empty")
  
  (check-error
   (rest  empty)
   "rest: expected argument of type <non-empty list>; given empty")
  )

;;; X represents
;;; Any type, such as Number, String, MachineState, etc.

;;; A ListOfX (LOX) is either
;;; -- empty
;;; -- (cons X ListOfX)

;;; TEMPLATE:
;;; lox-fn : ListOfX -> ??
;;; (define (lox-fn lst)
;;;   (cond
;;;     [(empty? lst) ...]
;;;     [else (... 
;;;             (first lst)
;;;             (lox-fn (rest lst)))]))

;;; EXAMPLES :
(define listofx1 (cons 1 empty))
(define listofx2 (cons "abc" (cons "de" empty)))
(define listofx3 (list machinestate1 machinestate2))

;;; Y represents
;;; Any type, such as Number, String, MachineState, etc.

;;; A ListOfY (LOY) is either
;;; -- empty
;;; -- (cons Y ListOfY)

;;; TEMPLATE:
;;; loy-fn : ListOfY -> ??
;;; (define (loy-fn lst)
;;;   (cond
;;;     [(empty? lst) ...]
;;;     [else (... 
;;;             (first lst)
;;;             (loy-fn (rest lst)))]))

;;; EXAMPLES :
(define listofy1 (cons 1 empty))
(define listofy2 (cons "abc" (cons "de" empty)))
(define listofy3 (list machinestate1 machinestate2))

;;; A ListOfString (LOSTR) is either
;;; -- empty
;;; -- (cons String ListOfString)

;;; TEMPLATE:
;;; lostr-fn : ListOfString -> ??
;;; (define (lostr-fn lst)
;;;   (cond
;;;     [(empty? lst) ...]
;;;     [else (... 
;;;             (first lst)
;;;             (lostr-fn (rest lst)))]))

;;; EXAMPLES :
(define string1 "a")
(define string2 "b")
(define listofstring1 (cons string1 empty))
(define listofstring2 (cons string1 (cons string2 empty)))
(define listofstirng3 (list string1 string2))

;;; TESTS :
(begin-for-test
  (check-equal? (empty? empty) true)
  (check-equal? (empty? (cons string1 empty)) false)
  (check-equal? (empty? (cons string2 (cons string1 empty))) false)
  
  (check-equal? (first (cons string1 empty)) string1)
  (check-equal? (rest  (cons string1 empty)) empty)
  
  (check-equal?
   (first (cons string2 (cons string1 empty)))
   string2)
  
  (check-equal?
   (rest  (cons string2 (cons string1 empty)))
   (cons string1 empty))
  
  (check-equal? listofstring2 listofstirng3)
  
  (check-error
   (first empty)
   "first: expected argument of type <non-empty list>; given empty")
  
  (check-error
   (rest  empty)
   "rest: expected argument of type <non-empty list>; given empty")
  )


;;; A List of Images (ListOfImage/LOI) is one of:
;;; -- empty
;;; -- (cons Image LOI)

;;; Template :
;;; loi-fn : LOI -> ??
;;; (define (loi-fn lst)
;;;   (cond
;;;     [(empty? lst) ...]
;;;     [else (... (first lst)
;;;                (loi-fn (rest lst)))]))

;;; Examples
(define image1 (circle 20 "outline" "blue"))
(define image2 (circle 10 "outline" "red"))

(define imagelist1 (cons image1 empty))
(define imagelist2 (cons image1 (cons image2 empty)))
(define imagelist3 (list image1 image2))


(begin-for-test
  (check-equal? (empty? empty) true)
  (check-equal? (empty? (cons image1 empty)) false)
  (check-equal? (empty? (cons image2 (cons image1 empty))) false)
  
  (check-equal? (first (cons image1 empty)) image1)
  (check-equal? (rest  (cons image1 empty)) empty)
  
  (check-equal? (first (cons image2 (cons image1 empty))) image2)
  (check-equal?
   (rest  (cons image2 (cons image1 empty)))
   (cons image1 empty))
  
  (check-equal? imagelist2 imagelist3)
  
  (check-error
   (first empty)
   "first: expected argument of type <non-empty list>; given empty")
  
  (check-error
   (rest  empty)
   "rest: expected argument of type <non-empty list>; given empty")
  )

;;; A List of POSNs (ListOfPOSN / LOP) is one of:
;;; -- empty
;;; -- (cons posn LOP)

;;; Template :
;;; lop-fn : LOP -> ??
;;; (define (lop-fn lst)
;;;   (cond
;;;     [(empty? lst) ...]
;;;     [else (... (first lst)
;;;                (lop-fn (rest lst)))]))

;;; Examples
(define posn1 (make-posn 20 -30))
(define posn2 (make-posn 0 200))

(define posnlist1 (cons posn1 empty))
(define posnlist2 (cons posn1 (cons posn2 empty)))
(define posnlist3 (list posn1 posn2))


(begin-for-test
  (check-equal? (empty? empty) true)
  (check-equal? (empty? (cons posn1 empty)) false)
  (check-equal? (empty? (cons posn2 (cons posn1 empty))) false)
  
  (check-equal? (first (cons posn1 empty)) posn1)
  (check-equal? (rest  (cons posn1 empty)) empty)
  
  (check-equal? (first (cons posn2 (cons posn1 empty))) posn2)
  (check-equal?
   (rest  (cons posn2 (cons posn1 empty)))
   (cons posn1 empty))
  
  (check-equal? posnlist2 posnlist3)
  
  (check-error
   (first empty)
   "first: expected argument of type <non-empty list>; given empty")
  
  (check-error
   (rest  empty)
   "rest: expected argument of type <non-empty list>; given empty")
  )

;;; End Data Definition
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Section : w-machine and selected states & Generalization.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; world-machine : World -> ListOfMachineState
;;; GIVEN: a World w.
;;; RETURNS: the list of machine states being displayed in that world
;;;     (which might not be a machine as defined by is-machine?)
;;; EXAMPLE: (world-machine (initial-world 0)) => empty
;;; STRATEGY: Call a more general function.

;;; FUNCTION DEFINITION
(define (world-machine w)
  (machine-from-lomsg (world-lomsg w)))
;;; Tests
(begin-for-test
  (check-equal? (world-machine (initial-world 0)) empty "No Machines present")
  (check-equal? (world-machine (initial-world 0)) empty "No Machines present"))


;;; machine-from-lomsg : ListOfMachineStateGUI -> ListOfMachineState
;;; GIVEN : A list of MachineStateGUI lomsg that is present in the World
;;; RETURNS : List Of MachineState in the lomsg.
;;; EXAMPLES : (machine-from-lomsg (world-lomsg (initial-world 0))) => '()
;;; STRATEGY : Apply HOF map on (ListOfMachineStateGUI) lomsg.

;;; FUNCTION DEFINITION
(define (machine-from-lomsg lomsg)
  (map
   machine-state-gui-ms
   lomsg))

;;; world-selected-states : World -> ListOfMachineState
;;; GIVEN: a World
;;; RETURNS: a list of the currently selected machine states
;;; EXAMPLE: (world-machine (initial-world 0)) => empty
;;; STRATEGY: Call a more general function.

;;; FUNCTION DEFINITION
(define (world-selected-states w)
  (selected-states-from-lomsg (world-lomsg w)))

;;; Tests
(begin-for-test
  (check-equal? (world-selected-states (initial-world 0)) empty
                "No Machines present")
  (check-equal? (world-selected-states (initial-world 0)) empty
                "No Machines present"))

;;; selected-states-from-lomsg : ListOfMachineStateGUI -> ListOfMachineState
;;; GIVEN : A list of MachineStateGUI lomsg that is present in the World
;;; RETURNS : A List Of selected MachineStates from the lomsg.
;;; EXAMPLES : (selected-states-from-lomsg (world-lomsg (initial-world 0)))
;;;             => '()
;;; STRATEGY : Use HOF filter on (ListOfMachineStateGUI) lomsg.

;;; FUNCTION DEFINITION
(define (selected-states-from-lomsg lomsg)
  (machine-from-lomsg
   (filter
    machine-state-gui-selected?
    lomsg)))

;;; make-normal-world : World -> World 
;;; GIVEN : A World w.
;;; RETURNS : A World with normal mode 
;;; EXAMPLES :
;;;  (make-normal-world (world-after-newkey-press (initial-world 0))))
;;;    => (make-world '() "Normal" "" '() '() 0 (make-posn 0 0))
;;; STRATEGY : Use template for World on w.

;;; FUNCTION DEFINITION
(define (make-normal-world w)
  (make-world (world-lomsg w) MODE-NORMAL "" '() '()
              (world-statecount w) (world-mouseprev w)))
;;; Tests:
(begin-for-test
  (check-equal? (make-normal-world (world-after-newkey-press
                                    (initial-world 0)))
                (make-world '() "Normal" "" '() '() 0 (make-posn 0 0))
                "World with normal mode"))

;;; make-world-with-newmode : World 1String -> World 
;;; GIVEN : A World w and the 1String to be appended with the name of the World.
;;; RETURNS : A World with new mode and the new name.
;;; EXAMPLES :
;;;  (make-world-with-newmode (initial-world 0) "t"))
;;;    => (make-world '() "New" "t" '() '() 0 (make-posn 0 0))
;;; STRATEGY : Use template for World on w.

;;; FUNCTION DEFINITION
(define (make-world-with-newmode w input)
  (make-world (world-lomsg w) MODE-NEW
              (string-append (world-name w) input)
              '() '()
              (world-statecount w) (world-mouseprev w)))
;;; Tests:
(begin-for-test
  (check-equal? (make-world-with-newmode  (initial-world 0) "t")
                (make-world '() "New" "t" '() '() 0 (make-posn 0 0))
                "World with normal mode"))


;;; is-mode-new? : World -> Boolean
;;; is-mode-normal? : World -> Boolean
;;; is-mode-trans? : World -> Boolean
;;; is-mode-Is? : World -> Boolean
;;; is-mode-c1? : World -> Boolean
;;; is-mode-c2? : World -> Boolean
;;; GIVEN : World w.
;;; RETURNS : True iff the mode of the world matches Corresponding Mode.
;;; EXAMPLES : (is-mode-new? INITIAL-WORLD) => #f
;;;   (is-mode-normal? INITIAL-WORLD) => #t
;;; STRATEGY : Use template for World on w.
(define (is-mode-new? w)
  (string=? (world-mode w) MODE-NEW))
(define (is-mode-normal? w)
  (string=? (world-mode w) MODE-NORMAL))
(define (is-mode-trans? w)
  (string=? (world-mode w) MODE-TRANS))
(define (is-mode-Is? w)
  (string=? (world-mode w) MODE-IS))
(define (is-mode-c1? w)
  (string=? (world-mode w) MODE-C1))
(define (is-mode-c2? w)
  (string=? (world-mode w) MODE-C2))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Section : Run functions
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;; run : PosReal -> World
;;; GIVEN: the number of seconds per tick
;;; EFFECT: runs the GUI, starting with the initial state
;;;     returned by initial-world
;;; RETURNS: the final state of the world
;;; EXAMPLES:
;;;     (run 1)
;;; STRATEGY : Use template for big-bang
(define (run num)
  (big-bang
   (initial-world 0)
   (on-mouse world-after-mouse-event)
   (on-key world-after-key-event)
   (on-draw world-to-scene)))

;;; initial-world : Any -> World
;;; GIVEN: any value (ignored)
;;; RETURNS: the initial world specified for the GUI
;;; EXAMPLE: (initial-world 0) = INITIAL-WORLD
;;; STRATEGY : Combine Simpler Function
(define (initial-world num)
  INITIAL-WORLD)
;;; TESTS
(begin-for-test
  (check-equal? (initial-world 0) INITIAL-WORLD "initialworld"))

;;; world-after-key-event : World KeyEvent -> WorldState
;;; GIVEN: a World and a KeyEvent
;;; RETURNS: the World that should follow the given World
;;;     after the given KeyEvent
;;; EXAMPLE :
;;;  (world-after-key-event INITIAL-WORLD "shift") = INITIAL-WORLD
;;;  (world-after-key-event INITIAL-WORLD "N")
;;;   => (make-world-with-new-mode INITIAL-WORLD "")
;;;  (all the single length keys are handled. Others are ignored.)
;;; STRATEGY : Cases on KeyEvent.

;;; FUNCTION DEFINITIONS :
(define (world-after-key-event w key)
  (cond
    [(> (string-length key) COUNT-ONE) w]
    [(key=? key NEWKEY) (world-after-newkey-press w)]
    [(key=? key TRANSKEY) (world-after-transkey-press w)]
    [(key=? key DELKEY) (world-after-delkey-press w)]
    [(key=? key SELKEY) (world-after-selkey-press w)]
    [(key=? key UNSELKEY) (world-after-unselkey-press w)]
    [(key=? key ACCKEY) (world-after-acckey-press w)]
    [(key=? key RETKEY) (world-after-retkey-press w)]
    [else (world-after-wildkey-press w key)]))

;;; TESTS :
(begin-for-test
  (check-equal? (world-after-key-event INITIAL-WORLD "shift") INITIAL-WORLD
                "ignore shift")
  (check-equal? (world-after-key-event INITIAL-WORLD "N")
                (make-world-with-newmode INITIAL-WORLD "")
                "New Mode")
  (check-equal? (world-after-key-event INITIAL-WORLD "T")
                (make-world '() "Trans" "" '() '() 0 (make-posn 0 0))
                "Trans Mode")
  (check-equal? (world-after-key-event (make-n-states 2) "S")
                (make-world
                 (list
                  (make-machine-state-gui
                   (make-machine-state "\u0001" #true #false '()) 50 50
                   #true)) "Normal" "" '() '() 1 (make-posn 0 0))
                "Select start state")
  (check-equal? (world-after-key-event
                 (world-after-key-event (make-n-states 2) "S") "U")
                (make-n-states 2)
                "Unselect all")
  (check-equal? (world-after-key-event
                 (world-after-key-event (make-n-states 2) "S") "A")
                (make-world
                 (list
                  (make-machine-state-gui
                   (make-machine-state "\u0001" #true #true '()) 50 50
                   #true)) "Normal" "" '() '() 1 (make-posn 0 0))
                "Acceptance toggled")
  (check-equal? (world-after-key-event
                 (world-after-key-event (make-n-states 2) "S") "D")
                (make-world '() "Normal" "" '() '() 1 (make-posn 0 0))
                "Delete selected states"))

;;; world-after-mouse-press : World Int Int MouseEvent -> World
;;; GIVEN: a World, the x- and y-coordinates of a mouse event, and the
;;;     mouse event
;;; RETURNS: the world that should follow the given world after the given
;;;     mouse event.
;;; EXAMPLE : See tests below. Mouse Events other than button-down, button-up
;;;           and drag are ignored.
;;; STRATEGY : Cases on MouseEvent.

;;; FUNCTION DEFINITION :
(define (world-after-mouse-event w mx my mev)
  (cond
    [(mouse=? mev PRESS) (world-after-mouse-press w mx my)]
    [(mouse=? mev RELEASE) (world-after-mouse-release w mx my)]
    [(mouse=? mev DRAG) (world-after-mouse-drag w mx my)]
    [else w]))

;;; TESTS :
(begin-for-test
  (check-equal? (world-after-mouse-event (make-n-states 2) 60 60 "button-down")
                (make-world
                 (list
                  (make-machine-state-gui
                   (make-machine-state "\u0001" #true #false '()) 50 50
                   #true)) "Normal" "" '() '() 1 (make-posn 60 60))
                "Select state 1")
  (check-equal? (world-selected-states (world-after-mouse-event
                                        (make-n-states 32) 60 60 "button-down"))
                (list
                 (make-machine-state "\u0001" #false #false '())
                 (make-machine-state "\u001F" #true #false '()))
                "2 states at 50,50 will be selected.")
  (check-equal? (world-after-mouse-event INITIAL-WORLD 600 600 "enter")
                INITIAL-WORLD "Ignore other key events")
  (check-equal? (world-after-mouse-event
                 (world-after-mouse-event (make-n-states 2) 60 60 "button-down")
                 70 70 "drag")
                (make-world
                 (list
                  (make-machine-state-gui
                   (make-machine-state "\u0001" #true #false '()) 60 60
                   #true)) "Normal" "" '() '() 1 (make-posn 70 70))
                "Select state 1")
  (check-equal? (world-after-mouse-event 
                 (make-world
                  (list
                   (make-machine-state-gui
                    (make-machine-state "\u0001" #true #false '()) 50 50
                    #true)) "Normal" "" '() '() 1 (make-posn 60 60))
                 60 60 "button-up")
                 (make-world
                  (list
                   (make-machine-state-gui
                    (make-machine-state "\u0001" #true #false '()) 50 50
                    #f)) "Normal" "" '() '() 1 (make-posn 60 60))))

;;; world-to-scene : World -> Scene
;;; GIVEN : a World
;;; RETURNS: a Scene that portrays the given World.
;;; EXAMPLES :
;;;   (world-to-scene INITIAL-WORLD)
;;;   => (empty-scene CANVAS-WIDTH CANVAS-HEIGHT
;;;                   (canvas-color-for-mode (world-mode INITIAL-WORLD)))
;;;   (world-to-scene world3)
;;;   => (place-image
;;;       (machine-state-gui-images gui3) 40 40
;;;       (empty-scene CANVAS-WIDTH CANVAS-HEIGHT NORMAL-MODE-CANVAS-COLOR))
;;; STRATEGY: Use template for World on w

;;; FUNCTION DEFINITION :
(define (world-to-scene w)
  (lomsg-transition-curves
   (world-lomsg w)
   (place-images
    (append
     (lomsg-images (world-lomsg w))
     (lomsg-state-gui-transition-input-symbol-images (world-lomsg w)))
    (append
     (lomsg-image-posns (world-lomsg w))
     (lomsg-state-gui-transition-input-symbol-image-posns (world-lomsg w)))
    (empty-scene CANVAS-WIDTH CANVAS-HEIGHT
                 (canvas-color-for-mode (world-mode w))))))

;;; TESTS :
(begin-for-test
  (check-equal?
   (world-to-scene INITIAL-WORLD)
   (empty-scene CANVAS-WIDTH CANVAS-HEIGHT
                 (canvas-color-for-mode (world-mode INITIAL-WORLD))))

  (check-equal?
   (world-to-scene world3)
   (place-image
    (machine-state-gui-images gui3) 40 40
    (empty-scene CANVAS-WIDTH CANVAS-HEIGHT NORMAL-MODE-CANVAS-COLOR)))
   )

;;; canvas-color-for-mode : Mode -> Color
;;; GIVEN : The current mode of the current world
;;; RETURNS : A color corresponding to the given mode.
;;; EXAMPLES : (canvas-color-for-mode "Normal") = NORMAL-MODE-CANVAS-COLOR
;;;            (canvas-color-for-mode "New") = NEW-MODE-CANVAS-COLOR
;;; STRATEGY : Use template for Mode on mode.
;;; FUNCTION DEFINITIONS : 
(define (canvas-color-for-mode mode)
  (cond [(string=? mode MODE-NORMAL) NORMAL-MODE-CANVAS-COLOR]
        [(string=? mode MODE-NEW) NEW-MODE-CANVAS-COLOR]
        [(string=? mode MODE-TRANS) TRANSITION-MODE-CANVAS-COLOR]
        [(string=? mode MODE-IS) TRANSITION-MODE-CANVAS-COLOR]
        [(string=? mode MODE-C1) TRANSITION-MODE-CANVAS-COLOR]
        [(string=? mode MODE-C2) TRANSITION-MODE-CANVAS-COLOR]))

;;; TESTS :
(begin-for-test
  (check-equal? (canvas-color-for-mode "Normal") NORMAL-MODE-CANVAS-COLOR
                "Normal mode color is returned")
  (check-equal? (canvas-color-for-mode "New") NEW-MODE-CANVAS-COLOR
                "New mode color is returned")
  (check-equal? (canvas-color-for-mode "Trans") TRANSITION-MODE-CANVAS-COLOR
                "Trans mode color is returned")
  (check-equal? (canvas-color-for-mode "IS") TRANSITION-MODE-CANVAS-COLOR
                "Trans mode color is returned")
  (check-equal? (canvas-color-for-mode "C1") TRANSITION-MODE-CANVAS-COLOR
                "Trans mode color is returned")
  (check-equal? (canvas-color-for-mode "C2") TRANSITION-MODE-CANVAS-COLOR
                "Trans mode color is returned"))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Section : machine-state-next
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;; machine-state-next : MachineState InputSymbol -> ListOfString
;;; GIVEN : a machine state and an InputSymbol
;;; RETURNS : the set of names (without duplicates, in any order)
;;;     for all machine states that can be reached by taking
;;;     one transition out of the given machine state that's labelled
;;;     by the given input symbol
;;; EXAMPLES :
;;;     (machine-state-next
;;;      (make-machine-state "Q5"
;;;                          false
;;;                          false
;;;                          (list (make-transition "a" "Q5")
;;;                                (make-transition "a" "Q7")
;;;                                (make-transition "b" "Q8")
;;;                                (make-transition "b" "Q5")))
;;;      "a") => some permutation of (list "Q5" "Q7")
;;;
;;;     (machine-state-next
;;;      (make-machine-state "Q5"
;;;                          false
;;;                          false
;;;                          (list (make-transition "a" "Q5")
;;;                                (make-transition "a" "Q7")
;;;                                (make-transition "b" "Q8")
;;;                                (make-transition "b" "Q5")))
;;;      "c") => empty
;;;     (machine-state-next s0 "a") => empty
;;;     (machine-state-next s1 "a") => (list "S0" "S1")
;;; STRATEGY : Use the tempate for MachineState on s

;;; FUNCTION DEFINITION :
(define (machine-state-next s input)
  (get-next-state-list (machine-state-transitions s) input))

;;; TESTS :
(begin-for-test
  (check-equal?
   (machine-state-next s0 "a")
   '()
   "Empty list")
  
  (check-equal?
   (machine-state-next s1 "a")
   (list "S0" "S1")
   "perm of S0 S1")
  
  (check-equal?
   (machine-state-next
    (make-machine-state "Q5"
                        false
                        false
                        (list (make-transition "a" "Q5")
                              (make-transition "a" "Q7")
                              (make-transition "b" "Q8")
                              (make-transition "b" "Q5")))
    "a")
   (list "Q5" "Q7")
   "perm of Q5 Q7")
  
  (check-equal?
   (machine-state-next
    (make-machine-state "Q5"
                        false
                        false
                        (list (make-transition "a" "Q5")
                              (make-transition "a" "Q7")
                              (make-transition "b" "Q8")
                              (make-transition "b" "Q5")))
    "c")
   empty
   "Empty list")
  )

;;; get-next-state-list : ListOfTransition InputSymbol -> ListOfString
;;; GIVEN : a list of transitions of a machine state and an InputSymbol
;;; RETURNS : the set of names (without duplicates, in any order)
;;;     for all machine states that can be reached by taking
;;;     one transition out of the machine state that's labelled
;;;     by the given input symbol
;;; EXAMPLES :
;;;     (get-next-state-list
;;;      (make-machine-state "Q5"
;;;                          false
;;;                          false
;;;                          (list (make-transition "a" "Q5")
;;;                                (make-transition "a" "Q7")
;;;                                (make-transition "b" "Q8")
;;;                                (make-transition "b" "Q5")))
;;;      "a") => some permutation of (list "Q5" "Q7")
;;;     (get-next-state-list '() "a") => emtpy
;;;     (get-next-state-list (machine-state-transitions s1) "a")
;;;     => (list "S0" "S1")
;;; STRATEGY : Use HOF filter for ListOfTransition on lot followed by map.

;;; FUNCTION DEFINITION :
(define (get-next-state-list lot input)
  (map
   transition-dest
   (filter
    ; :Transition -> Boolean
    ; RETURNS : whether the input matches with the label of the given
    ;     Transition.
    (lambda (t) (string=? input (transition-label t)))
    lot)))

;;; TESTS :
(begin-for-test
  (check-equal?
   (get-next-state-list '() "a")
   '()
   "Empty list")
  
  (check-equal?
   (get-next-state-list
    (machine-state-transitions s1) "a")
   (list "S0" "S1")
   "perm of S0 S1"))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Section : next-states
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;; next-states : ListOfMachineState InputSymbol -> ListOfString
;;; GIVEN : a list of machine states and an InputSymbol input
;;; RETURNS : the set of names (without duplicates, in any order)
;;;     for all machine states that can be reached by taking
;;;     one transition out of one of the listed machine states
;;;     that's labelled by the given input symbol
;;; EXAMPLES :
;;;     (next-states empty sym) => empty
;;;     (next-states (list state1) sym) => (machine-state-next state1 sym)
;;;     (next-states (list state1 state2) sym)
;;;         => a list representing the set union of
;;;                (machine-state-next state1 sym)
;;;            and (machine-state-next state2 sym)
;;;     (next-states (list s1 s2) "b") => (LIST "S2")
;;;     (next-states (list s1) "a") => (list "S0" "S1")

;;; STRATEGY  : Use HOF foldr for ListOfMachineState on lom.

;;; FUNCTION DEFINITION : 
(define (next-states lom input)
  (foldr
   ; :MachineState ListOfString -> ListOfString
   ; GIVEN : A MachineState m and ListOfString los.
   ; RETURNS : ListOfString (without Duplicates)
   ;          after the transition based on the input.
   (lambda (m los)  (union (machine-state-next m input) los))
   '()
   lom))

;;; TESTS:
(begin-for-test
  (check-equal? (next-states (list s1 s2) "b")
                (list "S2") "only S2")
  
  (check-equal?  (next-states (list s1) "a")
                 (list "S0" "S1")
                 "some permutation of (list S0 S1)")
  )

;;; union : ListOfString ListOfString -> ListOfString
;;; GIVEN: Two ListOfString los1 and los2.
;;; RETURNS: Union of the two lists without duplicates.
;;; EXAMPLES:
;;;  (union '("A" "B" "C") '("A" "D")) => '("B" "C" "A" "D")
;;;  (union '() '("A" "C")) => '("A" "C")
;;; STRATEGY : Use HOF filter for ListOfString on los1.

;;; FUNCTION DEFINITION : 
(define (union los1 los2)
  (append
   (filter
    ; :String -> Boolean
    ; RETURNS : True iff the Given String is not a member of los2.
    (lambda (el) (not (member? el los2)))
    los1)
   los2))

;;; TESTS :
(begin-for-test
  (check-equal? (union '("A" "B" "C") '("A" "D")) '("B" "C" "A" "D") "Union")
  (check-equal? (union '() '("A" "C")) '("A" "C")
                "No Change after Union with an empty list"))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Section : is-machine?
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;; is-machine? : ListOfMachineState -> Boolean
;;; GIVEN : A list of machine states lom.
;;; RETURNS : True iff all of the following conditions are true:
;;;     none of the machine states has the same name
;;;     there is exactly one machine state in the list for which
;;;         machine-state-is-start? returns true
;;;     for every machine state in the list, every one of its transitions
;;;         represents a transition to some machine state in the list
;;; EXAMPLES :
;;;     (is-machine? empty) => false
;;;     (is-machine? (list (make-machine-state "S0" true false empty)))
;;;         => true

;;; STRATEGY : Combine Simpler Functions.

;;; FUNCTION DEFINITION : 
(define (is-machine? lom)
  (and
   (is-unique? lom)
   (is-start-unique? lom)
   (is-transition-correct? lom)
   (not (empty? lom))))

;;; TESTS :
(begin-for-test
  (check-equal? (is-machine? nfa1) #t "nfa1 is a Machine")
  (check-equal? (is-machine? (list s0 s1)) #f "Not a Machine"))


;;; is-unique? : ListOfMachineState -> Boolean
;;; GIVEN : A list of machine states lom.
;;; RETURNS : True if none of the machine states has the same name
;;; EXAMPLES :
;;;     (is-unique? nfa1) => #t
;;;     (is-unique? (list s1 s1) => #f
;;; STRATEGY : Use HOF andmap for ListOfMachineState on lom.

;;; FUNCTION DEFINITION :
(define (is-unique? lom)
  (andmap
   ; :MachineState -> Boolean
   ; RETURNS : True if the Given MachineState is an unique element in the lom.
   (lambda (m) (= COUNT-ONE (lom-same-states-counts lom m)))
   lom))

;;; TESTS :
(begin-for-test
  (check-equal? (is-unique? nfa1) #t "Each state of nfa1 is unique")
  (check-equal? (is-unique? (list s1 s1)) #f "The same name exists!")
  )

;;; lom-same-states-counts
;;;          : ListOfMachineState MachineState -> NonNegInt
;;; GIVEN : A list of machine states lom and a machine state m1 
;;; RETURNS : how many machine states have the same name as m1.
;;; EXAMPLES :
;;;     (lom-same-states-counts empty s0) => 0
;;;     (lom-same-states-counts nfa1 s0) => 1
;;;     (lom-same-states-counts (list s0 s0 s0) s0) => 3
;;; STRATEGY : Use HOF foldr for ListOfMachineState on lom.

;;; FUNCTION DEFINITION :
(define (lom-same-states-counts lom m1)
  (foldr
   ; :MachineState NonNegInt -> NonNegInt
   ; RETURNS : True iff the name of the Given MachineState matches name of m1.
   (lambda (m c)
     (if (string=? (machine-state-name m1) (machine-state-name m))
         (+ COUNT-ONE c)
         c))
   COUNT-ZERO
   lom))

;;; TESTS :
(begin-for-test
  (check-equal? (lom-same-states-counts empty s0) 0)
  (check-equal? (lom-same-states-counts nfa1 s0) 1)
  (check-equal? (lom-same-states-counts (list s0 s0 s0) s0) 3)
  )

;;; Duplicate/Extra function - just for demo.
;;; get-same-states-from-lom
;;;          : MachineState ListOfMachineState -> ListOfMachineState
;;; GIVEN: A machine state m1 and a list of machine states lom.
;;; RETURNS: A list of machine states that have the same name as m1.
;;; EXAMPLES:
;;;     (get-same-states-from-lom s0 nfa1) => (list s0)
;;;     (get-same-states-from-lom s2 nfa1) => (list s2)
;;; STRATEGY : Use HOF filter for ListOfMachineState on lom.

;;; FUNCTION DEFINITION :
(define (get-same-states-from-lom m1 lom)
  (filter
   ; :MachineState -> Boolean
   ; RETURNS : True iff the name of the Given MachineState matches name of m1.
   (lambda (m) (string=? (machine-state-name m1) (machine-state-name m)))
   lom))

;;; TESTS :
(begin-for-test
  (check-equal? (get-same-states-from-lom s0 nfa1) (list s0) "s0")
  (check-equal? (get-same-states-from-lom s2 nfa1) (list s2) "s2"))

;;; is-start-unique? : ListOfMachineState -> Boolean
;;; GIVEN : a list of machine states lom.
;;; RETURNS : True iff there is only one start state on the given list.
;;; EXAMPLES :
;;;     (is-start-unique? nfa1) => #t
;;;     (is-start-unique? (list s0 s0 s1)) => #f
;;; STRATEGY : combine simpler function.

;;; FUNCTION DEFINITION :
(define (is-start-unique? lom)
  (= COUNT-ONE (length
                (filter-matching-states lom machine-state-is-start?))))

;;; TESTS :
(begin-for-test
  (check-equal? (is-start-unique? nfa1) #t "only one start state in nfa1")
  (check-equal?
   (is-start-unique? (list s0 s0 s1))
   #f "There are two start states!"))


;;; is-transition-correct? : ListOfMachineState -> Boolean
;;; GIVEN: a list of machine states lom.
;;; RETURNS: True iff all the possible transitions
;;;          are subset of ListOfMachineState.
;;; EXAMPLES: (is-transition-correct? nfa1) => #t
;;;            (is-transition-correct? (list s0 s1)) => #f
;;; STRATEGY : Use HOF andmap for ListOfMachineState on lom.

;;; FUNCTION DEFINITION :
(define (is-transition-correct? lom)
  (andmap
   ; :MachineState -> Boolean.
   ; RETURNS : True iff the Transition of the MachineState are subset of lom.
   (lambda (m) (subset? (get-from-list (machine-state-transitions m)
                                       transition-dest)
                        (get-from-list lom
                                       machine-state-name)))
   lom))

;;; TESTS :
(begin-for-test
  (check-equal? (is-transition-correct? nfa1)
                #t "nfa1's transitions are right")
  (check-equal? (is-transition-correct? (list s0 s1))
                #f "Not right"))

;;; filter-matching-states
;;;        : ListOfMachineState (MachineState -> Boolean) -> ListOfMachineState
;;; GIVEN: A ListOfMachineState lom and a Function fn.
;;; RETURNS: ListOfMachineState after filtering lom by given function.
;;; EXAMPLES:
;;;      (filter-matching-states nfa1 machine-state-is-start?) => (list s0)
;;;      (filter-matching-states nfa1 machine-state-is-accepting?) => (list s1)
;;; STRATEGY : Use HOF filter on for ListOfMachineState on lom.

;;; FUNCTION DEFINITION :
(define (filter-matching-states lom fn)
  (filter
   fn
   lom))

;;; TESTS :
(begin-for-test
  (check-equal?
   (filter-matching-states nfa1 machine-state-is-start?)
   (list s0)
   "s0 is the start state of nfa1")
  
  (check-equal?
   (filter-matching-states nfa1 machine-state-is-accepting?)
   (list s1)
   "s1 is an accepting state of nfa1"))


;;; get-from-list : ListOfX (X -> Y) -> ListOfY
;;; GIVEN: A list of X and a Function.
;;; RETURNS: A list of Xs after mapping the Given Function on list elements.
;;; EXAMPLES:
;;;     (get-from-list (machine-state-transitions s1) transition-dest)
;;;     => (list "S0" "S1" "S2")
;;;     (get-from-list nfa1 machine-state-name)
;;;     => (list "S1" "S2" "S0")
;;; STRATEGY : Use HOF map on for ListOfX on lox.

;;; FUNCTION DEFINITION :
(define (get-from-list lox fn)
  (map
   fn
   lox))

;;; TESTS :
(begin-for-test
  (check-equal?
   (get-from-list (machine-state-transitions s1) transition-dest)
   (list "S0" "S1" "S2"))
  
  (check-equal?
   (get-from-list nfa1 machine-state-name)
   (list "S1" "S2" "S0")))

;;; subset? : ListOfX ListOfX -> Boolean.
;;; GIVEN : Two lists of Xs lox1 and lox2.
;;; RETURNS : True iff the first list is a subset of the second list.
;;; EXAMPLES :
;;;     (subset? '("A" "B" "C") '("A" "B" "C" "D")) = #t
;;;     (subset? '(1 2 3 5) '(1 2 3 4)) = #f
;;; STRATEGY : Use HOF andmap for ListOfX on lox1.

;;; FUNCTION DEFINITION :
(define (subset? lox1 lox2)
  (andmap
   ; :X -> Boolean.
   ; RETURNS : True if the Given element is a member of lox2.
   (lambda (el) (member? el lox2))
   lox1))

;;; TESTS :
(begin-for-test
  (check-equal?
   (subset? '("A" "B" "C") '("A" "B" "C" "D"))
   true
   "'('A' 'B' 'C') is a subset of '('A' 'B' 'C' 'D')")
  
  (check-equal?
   (subset? '(1 2 3 5) '(1 2 3 4))
   false
   "'(1 2 3 5 is not a subset of '(1 2 3 4)"))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Section : machine-accepts?
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;; machine-accepts? : Machine InputString -> Boolean
;;; GIVEN : A finite state machine and an input string
;;; RETURNS : True iff the set of machine states that can be reached
;;;     from its start state by following transitions allowed by the
;;;     machine on the given input string contains an accepting state
;;;     of the machine
;;; EXAMPLES :
;;;     (machine-accepts? nfa1 (list "b" "b" "b")) => false
;;;     (machine-accepts? nfa1 (list "b" "b" "a")) => false
;;;     (machine-accepts? nfa1 (list "b" "a" "a")) => true
;;; STRATEGY : Combine simpler functions

;;; FUNCTION DEFINITION :
(define (machine-accepts? lom losym)
  (is-acceptable? (filter-matching-states lom machine-state-is-start?)
                  losym
                  lom))

;;; TESTS :
(begin-for-test
  (check-equal? (machine-accepts? nfa1 (list "b" "b" "b")) #f "Not Acc")
  (check-equal? (machine-accepts? nfa1 (list "b" "b" "a")) #f "Not Acc")
  (check-equal? (machine-accepts? nfa1 (list "b" "a" "a")) #t "Acc"))


;;; is-acceptable? : ListOfMachineState InputString Machine -> Boolean
;;; GIVEN: a list of machine states that only contains the start state,
;;;     an input string, and a finite state machine. 
;;; RETURNS: true iff the set of machine states that can be reached
;;;     from the start state in the given list by following transitions
;;;     allowed by the machine on the given input string contains an accepting
;;;     state of the machine
;;; EXAMPLES:
;;;     (is-acceptable? (list s0) (list "b" "b" "a") nfa1 ) => false
;;;     (is-acceptable? (list s0) (list "b" "b" "b") nfa1 ) => false 
;;;     (is-acceptable? (filter-matching-states nfa1 machine-state-is-start?) 
;;;                     '("b" "a" "a") nfa1) => true
;;; STRATEGY : Combine simpler functions

;;; FUNCTION DEFINITION :
(define (is-acceptable? los losym lom)
  (not (empty? (filter-matching-states
                (machine-states-reached-from lom los losym)
                machine-state-is-accepting?))))

;;; TESTS :
(begin-for-test
  (check-equal?
   (is-acceptable? (list s0) (list "b" "b" "b") nfa1 ) #f "Not Acc")
  
  (check-equal?
   (is-acceptable? (list s0) (list "b" "b" "a") nfa1 ) #f "Not Acc")
  
  (check-equal?
   (is-acceptable?
    (filter-matching-states nfa1 machine-state-is-start?)
    (list "b" "a" "a") nfa1)
   #t "Acc"))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Section : machine-states-reached-from
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;; machine-states-reached-from :
;;;     Machine ListOfMachineState InputString -> ListOfMachineState
;;; GIVEN : a machine, a possibly empty list of machine states belonging
;;;     to the machine, and an input string
;;; RETURNS : the set of machine states (without duplicates, in any order)
;;;     that can be reached from the given machine states by following any
;;;     sequence of transitions that consume the entire input string and
;;;     are allowed by the machine on that input string
;;; EXAMPLES :
;;;     (machine-states-reached-from nfa1 (list s1 s2) empty) => (list s1 s2)
;;;     (machine-states-reached-from nfa1 (list s1 s2) (list "b")) => (list s2)
;;;     (machine-states-reached-from nfa1 (list s1 s2) (list "a"))
;;;     => (list s0 s1)
;;;     (machine-states-reached-from nfa1 (list s1 s2) (list "a" "b" "b"))
;;;     => (list s2)
;;; STRATEGY : Use HOF foldl on for InputString on losym.

;;; FUNCTION DEFINITION :
(define (machine-states-reached-from lom los losym)
  (foldl
   ; :InputSymbol ListOfMachineState -> ListOfMachinState
   ; GIVEN : An InputSymbol sym and a ListOfMachineState loacs.
   ; RETURNS : A ListOfMachineState that follows the transition based on sym.
   (lambda (sym loacs) (get-states-from-name (next-states loacs sym) lom))
   los 
   losym))

;;; TESTS :
(begin-for-test
  (check-equal? (machine-states-reached-from nfa1 (list s1 s2) empty)
                (list s1 s2) "Empty so no trans")
  (check-equal? (machine-states-reached-from nfa1 (list s1 s2) (list "b"))
                (list s2) "Ends in s2.")
  (check-equal? (machine-states-reached-from nfa1 (list s1 s2) (list "a"))
                (list s0 s1) "Some Permutation of s0 s1")
  
  (check-equal? (machine-states-reached-from
                 nfa1 (list s1 s2) (list "a" "b" "b"))
                (list s2) "Ends in s2"))

;;; get-states-from-name :
;;;     ListOfString ListOfMachineState -> ListOfMachineState
;;; GIVEN: A list of strings representing the names of machine states and
;;;      a list of machine states lom.
;;; RETURNS: A list of machine states belongs to lom which cotains the machine
;;;      states that has the same state-name as one of the given names.
;;; EXAMPLES:
;;;     (get-states-from-name '("S0" "S1") nfa1) = (list s1 s0)
;;;     (get-states-from-name '("S2") nfa1) = (list s2)
;;; STRATEGY : Use HOF foldr for ListOfString on los.

;;; FUNCTION DEFINITION :
(define (get-states-from-name los lom)
  (foldr
   ; :String ListOfMachineState -> ListOfMachineState.
   ; RETURNS : Finds a ListOfMachineState that has same state-name as the Given
   ;           string and it is appened with the Given ListOfMachineState.
   (lambda (s states) (append states (get-state s lom)))
   '()
   los))

;;; TESTS :
(begin-for-test
  (check-equal?
   (get-states-from-name '("S0" "S1") nfa1)
   (list s1 s0)
   "the name of s0 is S0, and that of s1 is S1")
  
  (check-equal?
   (get-states-from-name '("S2") nfa1)
   (list s2)
   "the name of s2 is S2"))


;;; get-state : String ListOfMachineState -> ListOfMachineState
;;; GIVEN : A name of a state and a list of machine states.
;;; RETURNS : A list of machine states that contains the machine states
;;;     corresponding to the given name
;;; EXAMPLES :
;;;     (get-state "S1" nfa1) => (list s1)
;;;     (get-state "S2" nfa1) => (list s2)
;;; STRATEGY : Use HOF filter on for ListOfMachineState on lom.

;;; FUNCTION DEFINITION :
(define (get-state sname lom)
  (filter
   ; :MachineState -> Boolean
   ; RETURNS : True iff the MachineState's state-name is same as sname
   (lambda (m) (string=? (machine-state-name m) sname))
   lom))

;;; TESTS :
(begin-for-test
  (check-equal?
   (get-state "S1" nfa1)
   (list s1)
   "the name of s1 is S1")
  
  (check-equal?
   (get-state "S2" nfa1)
   (list s2)
   "the name of s2 is S2"))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Section : World-key-press
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;; world-after-newkey-press : World -> World.
;;; GIVEN : A World w.
;;; RETURNS : A World w that follows the NewKey "N" press.
;;; EXAMPLES :
;;;  (world-after-newkey-press (world-after-newkey-press INITIAL-WORLD))
;;;   => (make-world '() "New" "N" '() '() 0 (make-posn 0 0))
;;; STRATEGY :  Cases on Mode.

;;; FUNCTION DEFINITION
(define (world-after-newkey-press w)
  (cond
    [(is-mode-normal? w)
     (make-world-with-newmode w "")]
    [(is-mode-new? w)
     (make-world-with-newmode w NEWKEY)]
    [else w]))

;;; TESTS :
(begin-for-test
  (check-equal? (world-after-newkey-press
                 (world-after-newkey-press INITIAL-WORLD))
                (make-world '() "New" "N" '() '() 0 (make-posn 0 0))
                "world-at-new-mode")
  (check-equal? (world-after-newkey-press
                 (world-after-key-event INITIAL-WORLD "T"))
                (make-world '() "Trans" "" '() '() 0 (make-posn 0 0))
                "Trans mode."))

;;; world-after-delkey-press : World -> World.
;;; GIVEN : A World w.
;;; RETURNS : A World that follows the delete key press.
;;; EXAMPLES : See tests below.
;;; STRATEGY : Cases on Mode.

;;; FUNCTION DEFINITION :
(define (world-after-delkey-press w) 
  (cond
    [(or (is-mode-Is? w)
         (is-mode-c1? w)) w]
    [(is-mode-new? w)
     (make-world-with-newmode w DELKEY)]
    [else (world-after-delete w (world-selected-states w) )]))

;;; TESTS :
(begin-for-test
  (check-equal? (world-after-delkey-press
                 (world-after-key-event
                  (world-after-key-event INITIAL-WORLD "T") "a"))
                (make-world '() "IS" "a" '() '() 0 (make-posn 0 0))
                "IS mode, so same.")
  (check-equal? (world-after-delkey-press
                 (world-after-key-event
                  INITIAL-WORLD "N"))
                (make-world '() "New" "D" '() '() 0 (make-posn 0 0))
                "New mode, so append D with name."))
  
;;; world-after-delete : World ListOfMachineState -> World.
;;; GIVEN : A World w and a ListOfMachineState that to be deleted.
;;; RETURNS : A World with the lom removed.
;;; EXAMPLES : (world-after-delete (make-n-states 2)
;;;                                (world-machine (make-n-states 2)))
;;;  => (make-world '() "Normal" "" '() '() 1 (make-posn 0 0))
;;; STRATEGY : Use template for World on w.

;;; FUNCTION DEFINITION :
(define (world-after-delete w lom)
  (make-world
   (lomsg-after-trans-update
    (lomsg-after-delete (world-lomsg w) lom)
    (get-from-list lom machine-state-name))
   (world-mode w)
   (world-name w)
   (world-srcstate w)
   (world-dststate w)
   (world-statecount w)
   (world-mouseprev w)))

;;; TESTS :
(begin-for-test
  (check-equal? 
   (world-after-delete 2S2T (list (machine-state-gui-ms
                                   (first
                                    (world-lomsg 2S2T)))))
   (make-world
    (list
     (make-machine-state-gui
      (make-machine-state "\u0002" #true #false '()) 50 50 #false))
    "Normal" "" '() '() 2 (make-posn 150 50)) "Delete and update transitions"))



;;; lomsg-after-delete : ListOfMachineStateGUI ListOfMachineState
;                           -> ListOfMachineStateGUI
;;; GIVEN : A ListOfMachineStateGUI lomsg and a ListOfMachineState lom.
;;; RETURNS : ListOfMachineStateGUI after deleting lom from the given lomsg.
;;; EXAMPLES : (lomsg-after-delete (world-lomsg (make-n-states 2))
;;;                      (world-machine (make-n-states 2)))
;;;   => '()
;;; (lomsg-after-delete (world-lomsg (make-n-states 2))  '())
;;; => (list
;;;      (make-machine-state-gui
;;;       (make-machine-state "\u0001" #true #false '())  50 50 #false))
;;; STRATEGY : Use HOF filter on lomsg (ListOfMachineStateGUI).

;;; FUNCTION DEFINITION
(define (lomsg-after-delete lomsg lom)
  (filter
   ; : MachineStateGUI -> Boolean
   ; RETURNS : True iff the MachineState in given MachineStateGUI
   ;           is a member of lom.
   (lambda (msg) (not (member? (machine-state-gui-ms msg) lom)))
   lomsg))

 
;;; lomsg-after-trans-update : ListOfMachineStateGUI ListOfString
;;;                               -> ListOfMachineStateGUI
;;; GIVEN :  A ListOfMachineStateGUI lomsg and a ListOfString los that consist
;;;          of deleted states names.
;;; RETURNS : ListOfMachineStateGUI after deleting transitions that has dest as
;;;           one of the deleted states names los from the given lomsg.
;;; EXAMPLES :
;;;  (lomsg-after-trans-update (world-lomsg 2S2T) (list "\u0001"))
;;;   =>
;;; (list
;;;  (make-machine-state-gui
;;;   (make-machine-state "\u0001" #false #false
;;;    (list (make-transition "a" "\u0002"))) 150 50 #false)
;;;  (make-machine-state-gui
;;;   (make-machine-state "\u0002" #true #false '()) 50 50 #false))
;;; STRATEGY : Use HOF map on lomsg (ListOfMachineStateGUI).

;;; FUNCTION DEFINITION
(define (lomsg-after-trans-update lomsg los)
  (map
   ; : MachineStateGUI -> MachineStateGUI.
   ; RETURNS : Transitions of the Given MachineStateGUI updated and returned.   
   (lambda (msg) (msg-after-trans-update msg los))
   lomsg))

;;; msg-after-trans-update : MachineStateGUI ListOfString -> MachineStateGUI
;;; GIVEN : A MachineStateGUI msg and a ListOfString los that consist
;;;          of deleted states names.
;;; RETURNS : msg after deleting transitions that has dest as
;;;           one of the deleted states names los
;;; EXAMPLES : (msg-after-trans-update (first (world-lomsg 2S2T))
;;;                                    (list "\u0001"))
;;; => (make-machine-state-gui
;;;     (make-machine-state
;;;       "\u0001" #false #false
;;;     (list (make-transition "a" "\u0002"))) 150 50 #false)
;;; STRATEGY : Use template for MachineStateGUI on msg.

;;; FUNCTION DEFINITION
(define (msg-after-trans-update msg los)
  (make-machine-state-gui
   (ms-after-trans-update (machine-state-gui-ms msg) los)
   (machine-state-gui-x msg)
   (machine-state-gui-y msg)
   (machine-state-gui-selected? msg)))

;;; ms-after-trans-update : MachineState ListOfString -> MachineState.
;;; GIVEN : A MachineState ms and a ListOfString los that consist
;;;          of deleted states names.
;;; RETURNS : ms after deleting transitions that has dest as
;;;           one of the deleted states names los.
;;; EXAMPLES :
;;; (ms-after-trans-update (first (world-machine 2S2T)) (list "\u0001"))
;;; => (make-machine-state "\u0001" #f #f (list (make-transition "a" "\u0002")))
;;; STRATEGY : Use template for MachineState on ms.

;;; FUNCTION DEFINITION
(define (ms-after-trans-update ms los)
  (make-machine-state
   (machine-state-name ms)
   (machine-state-is-start? ms)
   (machine-state-is-accepting? ms)
   (trans-update (machine-state-transitions ms) los)))

;;; trans-update : ListOfTransition ListOfString -> ListOfTransition
;;; GIVEN : A ListOfTransition and a ListOfString los that consist
;;;          of deleted states names. 
;;; RETURNS : ListOfTransition after deleting transitions that has dest as
;;;           one of the deleted states names los.
;;; EXAMPLES :
;;; (trans-update (machine-state-transitions
;;;               (first (world-machine 2S2T))) (list "\u0001"))
;;; => (list (make-transition "a" "\u0002"))
;;; STRATEGY : Use HOF filter on ListOfTransition lot.

;;; FUNCTION DEFINITION
(define (trans-update lot los)
  (filter
   ; : Transition -> Boolean.
   ; RETURNS : True iff the dest of the given transition is a member of the los.
   (lambda (t) (not (member? (transition-dest t) los)))
   lot))

;;; world-after-acckey-press : World -> World.
;;; GIVEN : A World w.
;;; RETURNS : A World that follows the ACCKEY key press.
;;; EXAMPLES :
;;; (world-after-acckey-press (make-n-states 2)) =>
;;; (make-world
;;;  (list
;;;   (make-machine-state-gui
;;;    (make-machine-state "\u0001" #true #false '()) 50 50 #false))
;;;   "Normal" "" '() '() 1 (make-posn 0 0))
;;; STRATEGY : Cases on Mode.

;;; FUNCTION DEFINITION
(define (world-after-acckey-press w)
  (cond
    [(or (is-mode-Is? w)
         (is-mode-c1? w)) w]
    [(is-mode-new? w)
     (make-world-with-newmode w ACCKEY)]
    [else (world-with-accept-toggled w (world-selected-states w))]))

;;; TESTS :
(begin-for-test
  (check-equal? (world-after-acckey-press
                 (world-after-newkey-press 2STATES))
                (make-world
                 (list
                  (make-machine-state-gui
                   (make-machine-state "\u0001" #false #false '()) 150 50 #f)
                  (make-machine-state-gui
                   (make-machine-state "\u0002" #true #false '()) 50 50 #f))
                 "New" "A" '() '() 2
                 (make-posn 0 0)))
  (check-equal? (world-after-acckey-press
                 (world-after-mouse-event                 
                 (world-after-key-event
                  (world-after-key-event 2STATES "T")
                  "a") 50 50 "button-down"))
                (make-world
                 (list
                  (make-machine-state-gui
                   (make-machine-state "\u0001" #false #false '()) 150 50 #f)
                  (make-machine-state-gui
                   (make-machine-state "\u0002" #true #false '()) 50 50 #f))
                 "C1" "a" (make-machine-state "\u0002" #true #false '()) '() 2
                 (make-posn 50 50))))

;;; world-with-accept-toggled : World ListOfMachineState -> World.
;;; GIVEN : A World w and a ListOfMachineState lom.
;;; RETURNS : A World with is-accepting? toggled for the given lom.
;;; EXAMPLES :
;;;(world-with-accept-toggled 2STATES (world-machine 2STATES))
;;;(make-world
;;; (list
;;;  (make-machine-state-gui
;;;   (make-machine-state "\u0001" #false #true '()) 150 50 #f)
;;;  (make-machine-state-gui
;;;   (make-machine-state "\u0002" #true #true '()) 50 50 #f)
;;; "Normal" "" '() '() 2 (make-posn 0 0))
;;; STRATEGY : Use template for World on w.

;;; FUNCTION DEFINITION
(define (world-with-accept-toggled w lom)
  (make-world
   (lomsg-after-accept-toggled (world-lomsg w) lom)
   (world-mode w)
   (world-name w)
   (world-srcstate w)
   (world-dststate w)
   (world-statecount w)
   (world-mouseprev w)))

;;; lomsg-after-accept-toggled : ListOfMachineStateGUI ListOfMachineState
;;;                               -> ListOfMachineStateGUI
;;; GIVEN : A ListOfMachineStateGUI lomsg and a ListOfMachineState lom.
;;; RETURNS : lomsg with the acceptance of all of the given lom toggled.
;;; EXAMPLES :
;;;(lomsg-after-accept-toggled (world-lomsg 2STATES) (world-machine 2STATES)) =>
;;;(list
;;; (make-machine-state-gui
;;;  (make-machine-state "\u0001" #false #true '()) 150 50 #f)
;;; (make-machine-state-gui
;;;  (make-machine-state "\u0002" #true #true '()) 50 50 #f))
;;; STRATEGY : Use HOF map on lomsg.

;;; FUNCTION DEFINITION
(define (lomsg-after-accept-toggled lomsg lom)
  (map
   ; : MachineStateGUI -> MachineStateGUI.
   ; RETURNS : Acceptance of the Given MachineStateGUI toggled and returned.
   (lambda (msg) (msg-after-accept-toggled msg lom))
  lomsg))
;;; Tests
(begin-for-test
  (check-equal?
   (lomsg-after-accept-toggled (world-lomsg 2STATES)
                               (list
                                (make-machine-state "\u0001" #false #true '())))
   (list
    (make-machine-state-gui
     (make-machine-state "\u0001" #false #false '()) 150 50 #f)
    (make-machine-state-gui
     (make-machine-state "\u0002" #true #false '()) 50 50 #f))
   "Toggle acceptance of first state"))

;;; msg-after-accept-toggled :  MachineStateGUI ListOfMachineState
;;;                               -> MachineStateGUI
;;; GIVEN : A MachineStateGUI msg and a ListOfMachineState lom.
;;; RETURNS : msg with the acceptance toggled if it is a member of lom.
;;; EXAMPLES :
;;;(msg-after-accept-toggled (first (world-lomsg 2STATES))
;;;    (list (make-machine-state "\u0001" #false #true '())))
;;;(make-machine-state-gui
;;; (make-machine-state "\u0001" #false #false '()) 150 50 #f)
;;; STRATEGY : Use template for MachineStateGUI on msg.

;;; FUNCTION DEFINITION
(define (msg-after-accept-toggled msg lom)
  (if (member? (machine-state-gui-ms msg) lom)
      (toggle-acceptance msg)
      msg))

;;; toggle-acceptance : MachineStateGUI -> MachineStateGUI
;;; GIVEN : A MachineStateGUI msg.
;;; RETURNS : msg with acceptance toggled.
;;; EXAMPLES :
;;;(toggle-acceptance (first (world-lomsg 2STATES))) =>
;;;(make-machine-state-gui
;;; (make-machine-state "\u0001" #false #true '()) 150 50 #f)
;;; STRATEGY : Use template for MachineStateGUI on msg.

;;; FUNCTION DEFINITION
(define (toggle-acceptance msg)
  (make-machine-state-gui
   (ms-after-acceptance-toggled (machine-state-gui-ms msg))
   (machine-state-gui-x msg)
   (machine-state-gui-y msg)
   (machine-state-gui-selected? msg)))

;;; ms-after-acceptance-toggled : MachineState -> MachineState.
;;; GIVEN : A MachineState ms.
;;; RETURNS : ms with is-accepting? toggled.
;;; EXAMPLES :
;;;(ms-after-acceptance-toggled (make-machine-state "a" #false #true '()))
;;;=> (make-machine-state "a" #false #false '())
;;; STRATEGY : Use template for MachineState on ms.

;;; FUNCTION DEFINITION
(define (ms-after-acceptance-toggled ms)
  (make-machine-state
   (machine-state-name ms)
   (machine-state-is-start? ms)
   (not (machine-state-is-accepting? ms))
   (machine-state-transitions ms)))


;;; world-after-selkey-press : World -> World.
;;; GIVEN : A World w.
;;; RETURNS : World that follows the SELKEY press.
;;; EXAMPLES :
;;;(world-after-selkey-press
;;;                 (world-after-newkey-press 2STATES)) =>
;;;                (make-world
;;;                 (list
;;;                  (make-machine-state-gui
;;;                   (make-machine-state "\u0001" #false #false '()) 150 50 #f)
;;;                  (make-machine-state-gui
;;;                   (make-machine-state "\u0002" #true #false '()) 50 50 #f))
;;;                 "New" "S" '() '() 2
;;;                 (make-posn 0 0))
;;; STRATEGY : Cases on Mode.

;;; FUNCTION DEFINITION
(define (world-after-selkey-press w)
  (cond
    [(or (is-mode-Is? w)
         (is-mode-c1? w)) w]
    [(is-mode-new? w)
     (make-world-with-newmode w SELKEY)]
    [else (world-with-selected-states w
                                      (filter-matching-states
                                       (world-machine w)
                                       machine-state-is-start?))]))
;;; TESTS
(begin-for-test
  (check-equal? (world-after-selkey-press
                 (world-after-newkey-press 2STATES))
                (make-world
                 (list
                  (make-machine-state-gui
                   (make-machine-state "\u0001" #false #false '()) 150 50 #f)
                  (make-machine-state-gui
                   (make-machine-state "\u0002" #true #false '()) 50 50 #f))
                 "New" "S" '() '() 2
                 (make-posn 0 0))
                "New world with S appended to name.")
  (check-equal? (world-after-selkey-press
                 (world-after-mouse-event                 
                 (world-after-key-event
                  (world-after-key-event 2STATES "T")
                  "a") 50 50 "button-down"))
                (make-world
                 (list
                  (make-machine-state-gui
                   (make-machine-state "\u0001" #false #false '()) 150 50 #f)
                  (make-machine-state-gui
                   (make-machine-state "\u0002" #true #false '()) 50 50 #f))
                 "C1" "a" (make-machine-state "\u0002" #true #false '()) '() 2
                 (make-posn 50 50))
                "Ignore"))

;;; world-after-unselkey-press : World -> World.
;;; GIVEN : A World w.
;;; RETURNS : World that follows the UNSELKEY press.
;;; EXAMPLES : See tests below.
;;; STRATEGY : Cases on Mode.

;;; FUNCTION DEFINITION
(define (world-after-unselkey-press w)
  (cond
    [(or (is-mode-Is? w)
         (is-mode-c1? w)) w]
    [(is-mode-new? w)
     (make-world-with-newmode w UNSELKEY)]
    [else (world-with-selected-states w '())]))

;;; TESTS :
(begin-for-test
  (check-equal? (world-after-unselkey-press
                 (world-after-newkey-press 2STATES))
                (make-world
                 (list
                  (make-machine-state-gui
                   (make-machine-state "\u0001" #false #false '()) 150 50 #f)
                  (make-machine-state-gui
                   (make-machine-state "\u0002" #true #false '()) 50 50 #f))
                 "New" "U" '() '() 2
                 (make-posn 0 0))
                "New world with S appended to name.")
  (check-equal? (world-after-unselkey-press
                 (world-after-mouse-event                 
                 (world-after-key-event
                  (world-after-key-event 2STATES "T")
                  "a") 50 50 "button-down"))
                (make-world
                 (list
                  (make-machine-state-gui
                   (make-machine-state "\u0001" #false #false '()) 150 50 #f)
                  (make-machine-state-gui
                   (make-machine-state "\u0002" #true #false '()) 50 50 #f))
                 "C1" "a" (make-machine-state "\u0002" #true #false '()) '() 2
                 (make-posn 50 50))
                "Ignore")
  )

;;; world-after-transkey-press : World -> World.
;;; GIVEN : A World w.
;;; RETURNS : World that follows the TRANSKEY press.
;;; EXAMPLES :
;;;(world-after-transkey-press 2STATES) =>
;;;(make-world
;;; (list
;;;  (make-machine-state-gui
;;;   (make-machine-state "\u0001" #false #false '()) 150 50 #f)
;;;  (make-machine-state-gui
;;;   (make-machine-state "\u0002" #true #false '()) 50 50 #f))
;;; "Trans" "" '() '() 2 (make-posn 0 0))
;;; STRATEGY : Use the template for World on w

;;; FUNCTION DEFINITION :
(define (world-after-transkey-press w)
  (cond
    [(is-mode-normal? w)
     (make-world (lomsg-with-selected-states
                  (world-lomsg w) '())
                 MODE-TRANS "" '() '() (world-statecount w)
                 (world-mouseprev w))]
    [(is-mode-new? w)
     (make-world-with-newmode w TRANSKEY)]
    [else w]))

;;; TESTS :
(begin-for-test
  (check-equal? (world-after-transkey-press
                 (world-after-newkey-press INITIAL-WORLD)) 
  (make-world '() "New" "T" '() '() 0 (make-posn 0 0))
  "T appended with name")
(check-equal? (world-after-transkey-press
               (world-after-key-event 
                 (world-after-transkey-press INITIAL-WORLD) "a")) 
  (make-world '() "IS" "a" '() '() 0 (make-posn 0 0))
  "Ignore"))

;;; world-after-retkey-press : World -> World.
;;; GIVEN : A World w.
;;; RETURNS : World that follows the RETKEY press.
;;; EXAMPLES : (world-after-retkey-press 2STATES) => 2STATES
;;;(world-after-retkey-press (world-after-key-event
;;;                           (world-after-key-event INITIAL-WORLD "N") "a")) =>
;;;(make-world
;;; (list
;;;  (make-machine-state-gui
;;;   (make-machine-state "a" #true #false '()) 50 50 #f)
;;; "Normal" "" '() '() 1
;;; (make-posn 0 0))
;;; STRATEGY : Use the template for World on w

;;; FUNCTION DEFINITION
(define (world-after-retkey-press w)
  (cond 
    [(is-mode-new? w)
     (world-with-new-state w (world-name w))]
    [else w]))

;;; world-with-new-state : World String -> World
;;; GIVEN: a World and the name of a new state
;;; WHERE: no state in the world already has that name
;;; RETURNS: the world that should result from typing "N"
;;;     followed by the name of the new state, followed by "\r"
;;; EXAMPLE:
;;;     (world-machine
;;;      (world-with-new-state (initial-world 17) "q0"))
;;;  => (list (make-machine-state "q0" true false empty))
;;; STRATEGY : Use template for World on w. 

;;; FUNCTION DEFINITION
(define (world-with-new-state w name)
  (make-world 
   (machine-state-gui-append
    name (world-statecount w) (world-lomsg w)
    (is-start-unique? (world-machine w)))
   MODE-NORMAL "" '() '()
   (+ (world-statecount w) 1)
   (world-mouseprev w)))

;;; world-with-new-transition :
;;;     World InputSymbol MachineState MachineState -> World
;;; GIVEN: a world, an input symbol, and machine states q1 and q2
;;; WHERE: q1 and q2 belong to (world-machine world),
;;;     and q1 doesn't already have a transition to q2 via the input symbol
;;; RETURNS: the world that should result from adding a transition
;;;     labelled by the given input symbol that takes q1 to q2
;;; EXAMPLE:
;;;(world-with-new-transition 2STATES "a" (first (world-machine 2STATES))
;;;                           (second (world-machine 2STATES))) =>
;;;(make-world
;;; (list
;;;  (make-machine-state-gui
;;;   (make-machine-state
;;;    "\u0001" #f #f
;;;    (list (make-transition "a" "\u0002"))) 150 50 #f)
;;;  (make-machine-state-gui
;;;   (make-machine-state "\u0002" #true #false '()) 50 50 #f))
;;; "Normal" "" '() '() 2
;;; (make-posn 0 0))
;;; STRATEGY : Use template for World on w.

;;; FUNCTION DEFINITION :
(define (world-with-new-transition w input q1 q2)
  (make-world
   (transition-append (world-lomsg w)
                      (make-transition input (machine-state-name q2))
                      q1)
   MODE-NORMAL "" '() '() (world-statecount w) (world-mouseprev w)))

;;; first-six-chars : String -> String.
;;; GIVEN : A String name.
;;; RETURNS : First 6 chars of the String.
;;; EXAMPLES : (first-six-chars "g") => g
;;;            (first-six-chars "google") => google
;;;            (first-six-chars "googling") => googli
;;; STRATEGY : Combine Simpler Functions.

;;; FUNCTION DEFINITION :
(define (first-six-chars name)
  (if (<= (string-length name) MAX-NAME-LEN) name
      (substring name COUNT-ZERO MAX-NAME-LEN)))

;;; TESTS :
(begin-for-test
  (check-equal? (first-six-chars "googling") "googli" "only 6 chars"))

;;; machine-state-gui-append :
;;; String NonNegInteger ListOfMachineStateGUI Boolean -> ListOfMachineStateGUI
;;; GIVEN : Name of the state, Number of states created so far, 
;;;         ListOfMachineStateGUI present in the world and the Boolean to denote
;;;         if start is unique.
;;; RETURNS : ListOfMachineStateGUI appended with newly created state gui.
;;; EXAMPLES :
;;;(machine-state-gui-append "a" 2 (world-lomsg INITIAL-WORLD) #t) =>
;;;(list
;;; (make-machine-state-gui
;;;  (make-machine-state "a" #true #false '()) 250 50 #f))
;;; STRATEGY : Combine Simpler functions.

;;; FUNCTION DEFINITION
(define (machine-state-gui-append name count lomsg unique?)
  (append (new-state name count (length lomsg) unique?) 
          lomsg))

;;; new-state : String NonNegInteger NonNegInteger Boolean -> MachineStateGUI.
;;; GIVEN : name, NoOfStates created, Number of present and Boolean to denote
;;;         start is unique or not.
;;; RETURNS : A MachineStateGUI constructed based on the input.
;;; EXAMPLES : (new-state "a" 2 0 #t) =>
;;;  (list (make-machine-state "a" #true #false '()) 250 50 #f))
;;; STRATEGY : Combine Simpler functions.

;;; FUNCTION DEFINITION
(define (new-state name count len unique?)
  (list (make-machine-state-gui
         (make-machine-state
          name
          (not (and (> len COUNT-ZERO) unique?))
          #f
          '())
         (calculate-x (+ (modulo count COUNT-THIRTY) COUNT-ONE))
         (calculate-y (+ (modulo count COUNT-THIRTY) COUNT-ONE))
         #f)))

;;; calculate-x : NonNegInteger -> NonNegInteger.
;;; GIVEN : Number of states created so far.
;;; RETURNS : X-coordinate of the new state's center.
;;; EXAMPLES : (calculate-x 0) => 50.
;;;             (calculate-x 29) => 550.
;;;             (calculate-x 30) => 50.
;;; STRATEGY : Combine Simpler functions.

;;; FUNCTION DEFINITION
(define (calculate-x num)
  (cond
    [(= (modulo num MAX-STATES-IN-ROW) COUNT-ZERO)
     (- (* MAX-STATES-IN-ROW COUNT-HUNDRED) INITIAL-X)]
    [else  (- (* (modulo num MAX-STATES-IN-ROW) COUNT-HUNDRED) INITIAL-X)]))

;;; calculate-y : NonNegInteger -> NonNegInteger.
;;; GIVEN : Number of states created so far.
;;; RETURNS : Y-coordinate of the new state's center.
;;; EXAMPLES : (calculate-y 0) => 50.
;;;             (calculate-y 29) => 450.
;;;             (calculate-y 30) => 50.
;;; STRATEGY : Combine Simpler functions.

;;; FUNCTION DEFINITION
(define (calculate-y num)
  (cond
    [(= (floor (/ num MAX-STATES-IN-ROW)) COUNT-ZERO) INITIAL-Y]
    [else  (- (* (ceiling (/ num MAX-STATES-IN-ROW))
                 COUNT-HUNDRED)
              INITIAL-Y)]))

;;; transition-append : ListOfMachineStateGUI Transition MachineState
;;;                     -> ListOfMachineStateGUI
;;; GIVEN : LOMSG, and a Tranition that to be appended with the srcstate which
;;;         is present in the given lomsg.
;;; RETURNS : lomsg with the transition appended with srcstate.
;;; EXAMPLES :
;;;(transition-append (world-lomsg 2STATES) (make-transition "a" "\u0002")
;;;                   (second (world-machine 2STATES))) =>
;;;(list
;;; (make-machine-state-gui
;;;  (make-machine-state "\u0001" #false #false '()) 150 50 #f)
;;; (make-machine-state-gui
;;;  (make-machine-state
;;;   "\u0002" #t #f
;;;   (list (make-transition "a" "\u0002"))) 50 50 #f))
;;; STRATEGY : Use HOF map on lomsg (ListOfMachineStateGUI)

;;; FUNCTION DEFINITION
(define (transition-append lomsg trans srcstate)
  (map
   ; : MachineStateGUI -> MachineStateGUI.
   ; Returns : Transition appended with the srcstate present in given
   ;           MachineStateGUI.
   (lambda (msg) (if (equal? srcstate (machine-state-gui-ms msg))
                     (msg-with-new-transition msg trans srcstate)
                     msg))
   lomsg))

;;; msg-with-new-transition : MachineStateGUI Transition MachineState
;;;                     -> MachineStateGUI
;;; GIVEN : MSG, and a Tranition that to be appended with the srcstate which
;;;         is present in the given msg.
;;; RETURNS : msg with the transition appended with srcstate.
;;; EXAMPLES :
;;; (msg-with-new-transition (first (world-lomsg 2STATES) )
;;;                          (make-transition "a" "\u0002")
;;;                          (second (world-machine 2STATES))) =>
;;;(make-machine-state-gui
;;; (make-machine-state
;;;  "\u0002" #t #f
;;;  (list (make-transition "a" "\u0002"))) 150 50 #f)
;;; STRATEGY : Use template for MachineStateGUI on msg.

;;; FUNCTION DEFINITION
(define (msg-with-new-transition msg trans srcstate)
  (make-machine-state-gui (ms-with-new-transition srcstate trans)
                          (machine-state-gui-x msg)
                          (machine-state-gui-y msg)
                          (machine-state-gui-selected? msg)))

;;; ms-with-new-transition : MachineState Transition -> MachineState
;;; GIVEN : A MachineState ms and a Transition trans.
;;; RETURNS : MachineState with the given trans appended to it.
;;; EXAMPLES :
;;; STRATEGY : Use template for MachineState on ms.
;;;(ms-with-new-transition (second (world-machine 2STATES)) 
;;;                        (make-transition "a" "\u0002"))   =>
;;;(make-machine-state
;;; "\u0002" #t #f
;;; (list (make-transition "a" "\u0002")))

;;; FUNCTION DEFINITION :
(define (ms-with-new-transition ms trans)
  (make-machine-state (machine-state-name ms)
                      (machine-state-is-start? ms)
                      (machine-state-is-accepting? ms)
                      (cons trans (machine-state-transitions ms)))) 

;;; world-after-wildkey-press : World KeyEvent -> World.
;;; GIVEN : a World w and a KeyEvent key.
;;; RETURNS : World After the wildkey press.
;;; EXAMPLES :
;;;(world-after-wildkey-press INITIAL-WORLD "a") =>
;;;(make-world '() "Normal" "" '() '() 0 (make-posn 0 0))
;;; STRATEGY : Cases on Mode.

;;; FUNCTION DEFINITION :
(define (world-after-wildkey-press w key)
  (cond
    [(is-mode-new? w) 
     (make-world-with-newmode w key)]

    [(is-mode-normal? w) 
     (world-after-input-symbol w key)]
    
    [(is-mode-trans? w)
     (world-after-trans-input-symbol w key)]
    
    [else w]))

;;; TESTS :
(begin-for-test
  (check-equal? (world-after-wildkey-press INITIAL-WORLD "a") INITIAL-WORLD
                "ignore")
  (check-equal? (world-after-wildkey-press
                 (world-after-mouse-event (world-after-wildkey-press
                                           (world-after-transkey-press 2STATES)
                                           "a") 50 50 "button-down") "a")
                (make-world
                 (list
                  (make-machine-state-gui
                   (make-machine-state "\u0001" #false #false '()) 150 50 #f)
                  (make-machine-state-gui
                   (make-machine-state "\u0002" #true #false '()) 50 50 #f))
                 "C1"
                 "a"
                 (make-machine-state "\u0002" #true #false '())
                 '()
                 2
                 (make-posn 50 50))
                "Ignore"))

;;; world-after-input-symbol : World KeyEvent -> World.
;;; GIVEN : A World and a KeyEvent key.
;;; RETURNS : World that follows the key press.
;;; EXAMPLES : (world-after-input-symbol INITIAL-WORLD ",") => INITIAL-WORLD
;;; STRATEGY : Combine Simpler functions.

;;; FUNCTION DEFINITION :
(define (world-after-input-symbol w key)
  (if  (valid-input-symbol? (string->int key))
      (world-with-selected-states
       w
       (get-states-from-name (next-states (world-selected-states w) key)
                             (world-machine w)))
      w))

;;; TESTS :
(begin-for-test
  (check-equal? (world-after-input-symbol INITIAL-WORLD ",")
                INITIAL-WORLD "ignore"))

;;; world-after-trans-input-symbol : World KeyEvent -> World.
;;; GIVEN : A World and a KeyEvent key.
;;; RETURNS : World that follows the key press.
;;; EXAMPLES :
;;;  (world-after-trans-input-symbol INITIAL-WORLD "A") => INITIAL-WORLD
;;; STRATEGY : Use template for World on w.

;;; FUNCTION DEFINITION
(define (world-after-trans-input-symbol w key)
  (if (valid-input-symbol? (string->int key))
      (make-world
       (world-lomsg w) MODE-IS
       key '() '() (world-statecount w) (world-mouseprev w)) 
      (make-normal-world w)))
;;; TESTS
(begin-for-test
  (check-equal? (world-after-trans-input-symbol INITIAL-WORLD "A")
                INITIAL-WORLD "ignore"))

;;; valid-input-symbol? NonNegInteger -> Boolean
;;; GIVEN : An ascii-value.
;;; RETURNS : True if the value is between (97-122) or (48-57)
;;;           (i.e an InputSymbol)
;;; EXAMPLES : (valid-input-symbol? 98) = #t
;;;  (valid-input-symbol? 8) = #f
;;;  (valid-input-symbol? 49) = #t
;;; STRATEGY : Combine simpler functions.

;;; FUNCTION DEFINITION
(define (valid-input-symbol? ascii-value)
  (or (and (>= ascii-value ASCII-LOWER-A) (<= ascii-value ASCII-LOWER-Z))
      (and (>= ascii-value ASCII-ZERO) (<= ascii-value ASCII-NINE))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Section : world-mouse-events
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;; world-after-mouse-release : World Int Int -> World
;;; GIVEN : A World and the x,y coordinates of the mouse point.
;;; RETURNS : World that should follow then given one after the mouse release.
;;; EXAMPLES :
;;; (world-after-mouse-release 2STATES) => 2STATES
;;; STRATEGY : Cases on Mode

;;; FUNCTION DEFINITION :
(define (world-after-mouse-release w mx my)
  (cond
    [(is-mode-normal? w)
     (world-after-normal-mouse-release w)]
    [else w]))

;;; TESTS :
(begin-for-test
  (check-equal? (world-after-mouse-release 2STATES 50 50) 2STATES
                "Ignore")
  (check-equal? (world-after-mouse-release
                 (make-world
                  (list
                   (make-machine-state-gui
                    (make-machine-state "\u0001" #true #false '()) 50 50 #t))
                  "Normal" "" '()'() 2
                  (make-posn 40 40)) 50 50)
                 (make-world
                  (list
                   (make-machine-state-gui
                    (make-machine-state "\u0001" #true #false '()) 50 50 #f))
                  "Normal" "" '()'() 2
                  (make-posn 40 40))))

;;; world-after-normal-mouse-release : World -> World
;;; GIVEN : A World at Normal Mode. 
;;; RETURNS : World that should follow then given one after the mouse release.
;;; EXAMPLES :
;;;   (world-after-normal-mouse-release INITIAL-WORLD)
;;;   => INITIAL-WORLD
;;;   (world-after-normal-mouse-release world2)
;;;   => (make-world
;;;       (list (make-machine-state-gui
;;;             (machine-state-gui-ms machinestategui6) 60 20 false))
;;;       MODE-NORMAL "" '() '() COUNT-ONE (make-posn 0 0))
;;; STRATEGY : Use the template for World on w

;;; FUNCTION DEFINITION :
(define (world-after-normal-mouse-release w)
  (make-world (lomsg-after-mouse-release (world-lomsg w))
              (world-mode w)
              (world-name w)
              (world-srcstate w)
              (world-dststate w)
              (world-statecount w)
              (world-mouseprev w)))

;;; TESTS :
(begin-for-test
  (check-equal?
   (world-after-normal-mouse-release INITIAL-WORLD)
   INITIAL-WORLD)

  (check-equal?
   (world-after-normal-mouse-release world2)
   (make-world
    (list (make-machine-state-gui
           (machine-state-gui-ms machinestategui6) 60 20 false))
    MODE-NORMAL "" '() '() COUNT-ONE (make-posn 0 0)))
  )

;;; lomsg-after-mouse-release : ListOfMachineStateGUI -> ListOfMachineStateGUI
;;; GIVEN : A list of MachineStateGUIs
;;; RETURNS : A list of MachineStateGUIs that should follow then given one after
;;;     the mouse release.
;;; EXAMPLES :
;;;     (lomsg-after-mouse-release machinestateguilist4)
;;;     => machinestateguilist4
;;;     (lomsg-after-mouse-release (list machinestategui3 machinestategui6))
;;;     => (list machinestategui3
;;;             (make-machine-state-gui
;;;             (machine-state-gui-ms machinestategui6) 60 20 false))
;;; STRATEGY : Use HOF map for ListOfMachineStateGUI on lomsg

;;; FUNCTION DEFINITION :
(define (lomsg-after-mouse-release lomsg)
  (map
   ;;; MachineStateGUI -> MachineStateGUI
   ;;; GVIEN : A MachineStateGUI present in the current world state
   ;;; RETURNS : A MachineStateGUI that should follow then given one after
   ;;;     the mouse release.
   (lambda (msg) ( machine-state-gui-after-mouse-release msg))
   lomsg))

;;; TESTS :
(begin-for-test
  (check-equal?
   (lomsg-after-mouse-release machinestateguilist4)
   machinestateguilist4)

  (check-equal?
   (lomsg-after-mouse-release (list machinestategui3 machinestategui6))
   (list machinestategui3
         (make-machine-state-gui
          (machine-state-gui-ms machinestategui6) 60 20 false)))
  )

;;; machine-state-gui-after-mouse-release : MachineStateGUI -> MachineStateGUI
;;; GIVEN : A MachineStateGUI
;;; RETURNS : A MachineStateGUI that should follow then given of after the
;;;     mouse release.
;;; EXAMPLES :
;;;     (machine-state-gui-after-mouse-release machinestategui3 )
;;;     => machinestategui3
;;;     (machine-state-gui-after-mouse-release machinestategui6)
;;;     => (make-machine-state-gui
;;;         (machine-state-gui-ms machinestategui6) 60 20 false))
;;; STRATEGY : Use the template for MachineStateGUI on msg

;;; FUNCTION DEFINITION :
(define (machine-state-gui-after-mouse-release msg)
  (make-machine-state-gui (machine-state-gui-ms msg)
                          (machine-state-gui-x msg)
                          (machine-state-gui-y msg)
                          false))

;;; TESTS :
(begin-for-test
  (check-equal?
   (machine-state-gui-after-mouse-release machinestategui3 )
   machinestategui3)

  (check-equal?
   (machine-state-gui-after-mouse-release machinestategui6)
   (make-machine-state-gui
    (machine-state-gui-ms machinestategui6) 60 20 false))
  )

;;; world-after-mouse-press : World Int Int -> World
;;; GIVEN : A World and x,y coordinates of the Mouse point.
;;; RETURNS : World that should follow then given one after the mouse click.
;;; EXAMPLES : see tests below.
;;; STRATEGY : Cases on Mode.

;;; FUNCTION DEFINITION :
(define (world-after-mouse-press w mx my)
  (cond
    [(is-mode-normal? w)
     (world-after-normal-mouse-press w mx my)]
    
    [(is-mode-Is? w)
     (world-after-src-state-press w mx my)]
    
    [(is-mode-c1? w)
     (world-after-new-transition
      (world-after-dest-state-press w mx my))]
    
    [else w]))

;;; TESTS :
(begin-for-test
  (check-equal? (world-after-mouse-press (make-n-states 2) 50 50)
                (make-world
                 (list
                  (make-machine-state-gui
                   (make-machine-state "\u0001" #true #false '()) 50 50 #true))
                 "Normal" "" '() '() 1 (make-posn 50 50))
                "Select state 1")
  (check-equal? (world-after-mouse-press
                 (world-after-newkey-press (make-n-states 2)) 50 50)
                (world-after-newkey-press (make-n-states 2))
                "Ignore"))


;;; world-after-normal-mouse-press : World Int Int -> World
;;; GIVEN : A World at normal mode and the current x, y coordinates of the mouse
;;; RETURNS : World that should follow then given one after the mouse click.
;;; EXAMPLES :
;;;     (world-after-normal-mouse-press INITIAL-WORLD 0 0)
;;;     => INITIAL-WORLD
;;;     (world-after-normal-mouse-press world1 20 20)
;;;     => (make-world
;;;         (list (make-machine-state-gui
;;;               (make-machine-state
;;;                 "S0" true false
;;;                (list (make-transition "A" "S0")
;;;                      (make-transition "B" "S1"))) 
;;;           20 20 true)
;;;         machinestategui4
;;;         machinestategui5)
;;;        MODE-NORMAL "" '() '() COUNT-THREE (make-posn 20 20))
;;; STRATEGY : Use the template for World on w

;;; FUNCTION DEFINITION :
(define (world-after-normal-mouse-press w mx my)
  (make-world (lomsg-after-mouse-press (world-lomsg w) mx my)
              (world-mode w)
              (world-name w)
              (world-srcstate w)
              (world-dststate w)
              (world-statecount w)
              (make-posn mx my)))

;;; TESTS :
(begin-for-test
  (check-equal?
   (world-after-normal-mouse-press INITIAL-WORLD 0 0)
   INITIAL-WORLD)
  
  (check-equal?
   (world-after-normal-mouse-press world1 20 20)
   (make-world
    (list (make-machine-state-gui
           (make-machine-state
            "S0" true false
            (list (make-transition "A" "S0") (make-transition "B" "S1"))) 
           20 20 true)
          machinestategui4
          machinestategui5)
    MODE-NORMAL "" '() '() COUNT-THREE (make-posn 20 20)))
   )


;;; world-after-src-state-press : World Int Int -> World
;;; GIVEN : A World w and Mouse Coordinates x,y
;;; RETURNS : World that follows the srcstate press.
;;; EXAMPLES : (world-after-src-state-press 2S2T 400 400) = 2S2T
;;; STRATEGY : Use template for World on w.

;;; FUNCTION DEFINITION :
(define (world-after-src-state-press w mx my)
  (if (= COUNT-ONE (length (filter-matching-states
                             (lomsg-after-mouse-press (world-lomsg w) mx my)
                             machine-state-gui-selected?)))
      (world-with-src-state w mx my)
      (world-with-selected-states (make-normal-world w) '())))

;;; TESTS :
(begin-for-test
  (check-equal? (world-after-src-state-press 2S2T 400 400) 2S2T "ignore"))


;;; world-with-src-state : World Int Int -> World.
;;; GIVEN : A World w and Mouse Coordinates x,y
;;; RETURNS : World with the srcstate updated.
;;; EXAMPLES : (world-with-src-state 2S2T 50 50) = 2S2T
;;; STRATEGY : Use template for World on w.

;;; FUNCTION DEFINITION :
(define (world-with-src-state w mx my)
  (make-world (lomsg-after-mouse-release (world-lomsg w))
              MODE-C1
              (world-name w)
              (machine-state-gui-ms
               (first (filter-matching-states
                       (lomsg-after-mouse-press (world-lomsg w) mx my)
                       machine-state-gui-selected?)))
              '()
              (world-statecount w)
              (make-posn mx my)))

;;; world-after-new-transition : World -> World.
;;; GIVEN : A World.
;;; RETURNS : A World with new Transition created from
;;;           name, srcstate and dststate
;;; EXAMPLES :
;;; (world-after-new-transition 2STATES) = 2STATES
;;; STRATEGY : Use template for World on w.

;;; FUNCTION DEFINITION :
(define (world-after-new-transition w)
  (if (is-mode-c2? w)
      (world-with-new-transition
       w (world-name w) (world-srcstate w) (world-dststate w))
      w))

;;; TESTS :
(begin-for-test
  (check-equal? (world-after-new-transition 2STATES)
                2STATES "Ignore"))

;;; world-after-dest-state-press : World Int Int -> World
;;; GIVEN : A World w and Mouse Coordinates x,y
;;; RETURNS : World that follows the dststate press.
;;; EXAMPLES :
;;; (world-after-dest-state-press 2STATES 300 300) = 2STATES
;;; STRATEGY : Use template for World on w.

;;; FUNCTION DEFINITION :
(define (world-after-dest-state-press w mx my)
  (if (= COUNT-ONE (length (filter-matching-states
                             (lomsg-after-mouse-press (world-lomsg w) mx my)
                             machine-state-gui-selected?)))
      (world-with-dest-state w mx my)
      (world-with-selected-states (make-normal-world w) '())))

;;; TESTS :
(begin-for-test
  (check-equal? (world-after-dest-state-press 2STATES 300 300)
                2STATES "Ignore"))

;;; world-with-dest-state : World Int Int -> World.
;;; GIVEN : A World w and Mouse Coordinates x,y
;;; RETURNS : World with the dststate updated.
;;; EXAMPLES :
;;;(world-with-dest-state (make-world
;;; (list
;;;  (make-machine-state-gui
;;;   (make-machine-state "\u0002" #true #false '()) 50 50 #f))
;;; MODE-C1 "a" (make-machine-state "\u0002" #true #false '())
;;; '() 1 (make-posn 0 0)) 50 50) =>
;;;(make-world
;;; (list
;;;  (make-machine-state-gui
;;;   (make-machine-state "\u0002" #true #false '()) 50 50 #f)) "C2" "a"
;;; (make-machine-state "\u0002" #true #false '())
;;; (make-machine-state "\u0002" #true #false '()) 1 (make-posn 50 50))
;;; STRATEGY : Use template for World on w.

;;; FUNCTION DEFINITION :
(define (world-with-dest-state w mx my)
  (make-world (lomsg-after-mouse-release (world-lomsg w))
              MODE-C2
              (world-name w)
              (world-srcstate w) 
              (machine-state-gui-ms
               (first (filter-matching-states
                       (lomsg-after-mouse-press (world-lomsg w) mx my)
                       machine-state-gui-selected?)))
              (world-statecount w)
              (make-posn mx my)))


;;; lomsg-after-mouse-press :
;;;     ListOfMachineStateGUI Integer Integer -> ListOfMachineStateGUI
;;; GIVEN : A list of MachineStateGUIs and the x, y coordinates of the mouse.
;;; RETURNS : A list of MachineStateGUIs that should follow the given one after
;;;     the mouse click.
;;; EXAMPLES :
;;;     (lomsg-after-mouse-press
;;;      (list machinestategui3 machinestategui5) 25 23)
;;;     => (list (make-machine-state-gui
;;;               (machine-state-gui-ms machinestategui3) 20 20 true)
;;;               machinestategui5)
;;;     (lomsg-after-mouse-press
;;;      (list machinestategui4 machinestategui5) 25 23)
;;;     => (list machinestategui4 machinestategui5)
;;; STRATEGY : Use the template for ListOfMachineStateGUI on lomsg

;;; FUNCTION DEFINITION :
(define (lomsg-after-mouse-press lomsg mx my)
  (map
   ;;; MachineStateGUI -> MachineStateGUI
   ;;; GIVEN : A MachineStateGUI present in the current world state
   ;;; RETURNS : A MachineStateGUI that should follow the given one after
   ;;;     the mouse click.
   (lambda (msg) (machine-state-gui-after-mouse-press msg mx my))
   lomsg))

;;; TESTS :
(begin-for-test
  (check-equal?
   (lomsg-after-mouse-press
    (list machinestategui3 machinestategui5) 25 23)
   (list (make-machine-state-gui
          (machine-state-gui-ms machinestategui3) 20 20 true)
         machinestategui5))
  
  (lomsg-after-mouse-press
   (list machinestategui4 machinestategui5) 25 23)
  (list machinestategui4 machinestategui5)
  )


;;; machine-state-gui-after-mouse-press :
;;;     MachineStateGUI Int Int -> MachineStateGUI
;;; GIVEN : A MachineStateGUI and x, y coordinates of the mouse position.
;;; RETURNS : A MachineStateGUI that should follow the given of after the
;;;     mouse click.
;;; EXAMPLES :
;;;     (machine-state-gui-after-mouse-press machinestategui3 25 23)
;;;     => (make-machine-state-gui
;;;         (machine-state-gui-ms machinestategui3) 20 20 true)
;;;     (machine-state-gui-after-mouse-press machinestategui6 25 23)
;;;     => (make-machine-state-gui
;;;         (machine-state-gui-ms machinestategui6) 60 20 false)
;;; STRATEGY : Use the template for MachineStateGUI on msg

;;; FUNCTION DEFINITION :
(define (machine-state-gui-after-mouse-press msg mx my)
  (make-machine-state-gui (machine-state-gui-ms msg)
                          (machine-state-gui-x msg)
                          (machine-state-gui-y msg)
                          (mouse-cursor-within-machine-state-gui? msg mx my)))

;;; TESTS :
(begin-for-test
  (check-equal?
   (machine-state-gui-after-mouse-press machinestategui3 25 23)
   (make-machine-state-gui
    (machine-state-gui-ms machinestategui3) 20 20 true))

  (check-equal?
   (machine-state-gui-after-mouse-press machinestategui5 25 23)
   machinestategui5)
  )


;;; mouse-cursor-within-machine-state-gui? :
;;;     MachineStateGUI Integer Integer -> Boolean
;;; GIVEN : A MachineStateGUI and x, y coordinates of the mouse position.
;;; RETURNS : whether the mouse cursor is located within the the circle of the
;;;     given machine state gui.
;;; EXAMPLES :
;;;     (mouse-cursor-within-machine-state-gui? machinestategui3 25 23) => true
;;;     (mouse-cursor-within-machine-state-gui? machinestategui6 25 23) => false
;;; STRATEGY : Use the template for MachineStateGUI on msg

;;; FUNCTION DEFINITION :
(define (mouse-cursor-within-machine-state-gui? msg mx my)
  (>= OUTER-CIRCLE-RADIUS
      (sqrt(+ (sqr(- (machine-state-gui-x msg) mx))
              (sqr(- (machine-state-gui-y msg) my))))))

;;; TESTS :
(begin-for-test
  (check-equal?
   (mouse-cursor-within-machine-state-gui? machinestategui3 25 23)
   true)

  (check-equal?
   (mouse-cursor-within-machine-state-gui? machinestategui6 25 23)
   false)
  )

;;; world-after-mouse-drag : World Integer Integer -> World
;;; GIVEN : A World and the current x, y coordinates of the mouse.
;;; RETURNS : World that should follow then given one after the mouse drag.
;;; EXAMPLES :
;;;     (world-after-mouse-drag INITIAL-WORLD 0 0) => INITIAL-WORLD
;;;     (world-after-mouse-drag world2 65 65)
;;;     => (make-world
;;;         (list (make-machine-state-gui
;;;                (machine-state-gui-ms machinestategui6)
;;;                (+ 60 65) (+ 20 65) true))
;;;         MODE-NORMAL "" '() '() COUNT-ONE (make-posn 65 65))
;;; STRATEGY : Use the template for World on w

;;; FUNCTION DEFINITION :
(define (world-after-mouse-drag w mx my)
  (make-world (lomsg-after-mouse-drag
               (world-lomsg w)
               (- mx (posn-x (world-mouseprev w)))
               (- my (posn-y (world-mouseprev w))))
              (world-mode w)
              (world-name w)
              (world-srcstate w)
              (world-dststate w)
              (world-statecount w)
              (make-posn mx my)))

;;; TESTS :
(begin-for-test
  (check-equal?
   (world-after-mouse-drag INITIAL-WORLD 0 0) INITIAL-WORLD)
  
  (check-equal?
   (world-after-mouse-drag world2 65 65)
   (make-world
    (list (make-machine-state-gui
           (machine-state-gui-ms machinestategui6) (+ 60 65) (+ 20 65) true))
    MODE-NORMAL "" '() '() COUNT-ONE (make-posn 65 65)))
  )

;;; lomsg-after-mouse-drag :
;;;     ListOfMachineStateGUI Integer Integer -> ListOfMachineStateGUI
;;; GIVEN : A list of MachineStateGUIs and the x, y offsets between the previous
;;;     and current mouse positions.
;;; RETURNS : A list of MachineStateGUIs that should follow then given one after
;;;     the mouse drag.
;;; EXAMPLES :
;;;      (lomsg-after-mouse-drag machinestateguilist4 10 10)
;;;      => machinestateguilist4
;;;      (lomsg-after-mouse-drag (list machinestategui6) 0 0)
;;;      => (list machinestategui6)
;;;      (lomsg-after-mouse-drag (list machinestategui6) 5 5)
;;;      => (list (make-machine-state-gui
;;;               (machine-state-gui-ms machinestategui6) 75 25 true))
;;; STRATEGY : Use the template for ListOfMachineStateGUI on lomsg

;;; FUNCTION DEFINITION :
(define (lomsg-after-mouse-drag lomsg offsetx offsety)
  (map
   ;;; MachineStateGUI -> MachineStateGUI
   ;;; GIVEN : A MachineStateGUI
   ;;; RETURNS : A MachineStateGUI that should follow then given one after
   ;;;     the mouse drag.
   (lambda (msg) ( machine-state-gui-after-mouse-drag msg offsetx offsety))
   lomsg))

;;; TESTS :
(begin-for-test
  (check-equal?
   (lomsg-after-mouse-drag machinestateguilist4 10 10) machinestateguilist4)
  
  (check-equal?
   (lomsg-after-mouse-drag (list machinestategui6) 0 0) (list machinestategui6))
  
  (check-equal?
   (lomsg-after-mouse-drag (list machinestategui6) 5 5)
   (list (make-machine-state-gui
          (machine-state-gui-ms machinestategui6) (+ 60 5) (+ 20 5) true)))
  )

;;; machine-state-gui-after-mouse-drag :
;;;     MachineStateGUI Integer Integer -> MachineStateGUI
;;; GIVEN : A MachineStateGUI and the x, y offsets between the previous
;;;     and current mouse positions.
;;; RETURNS : A MachineStateGUI that should follow then given of after the
;;;     mouse drag.
;;; EXAMPLES :
;;;     (machine-state-gui-after-mouse-drag machinestategui3 10 10)
;;;     => machinestategui3
;;;     (machine-state-gui-after-mouse-drag machinestategui6 5 5)
;;;     => (make-machine-state-gui
;;;         (machine-state-gui-ms machinestategui6) (+ 60 5) (+ 20 5) true)
;;; STRATEGY : Use the template for MachineStateGUI on msg

;;; FUNCTION DEFINITION :
(define (machine-state-gui-after-mouse-drag msg offsetx offsety)
  (cond [(false? (machine-state-gui-selected? msg)) msg]
        [else (make-machine-state-gui
               (machine-state-gui-ms msg)
               (+ (machine-state-gui-x msg) offsetx)
               (+ (machine-state-gui-y msg) offsety)
               (machine-state-gui-selected? msg))]))

;;; TESTS :
(begin-for-test
  (check-equal?
   (machine-state-gui-after-mouse-drag machinestategui3 10 10)
   machinestategui3)

  (check-equal?
   (machine-state-gui-after-mouse-drag machinestategui6 5 5)
   (make-machine-state-gui
    (machine-state-gui-ms machinestategui6) (+ 60 5) (+ 20 5) true))
  )

;;; world-with-selected-states : World ListOfMachineState -> World
;;; GIVEN: a world and a list containing some of its machine states
;;; WHERE: the list is a set (contains no duplicates)
;;; RETURNS: a world like the given world but with the given set
;;;     of machine states as its set of selected states
;;; EXAMPLE:
;;;     if q1 and q2 belong to (world-machine world), then
;;;     (world-selected-states
;;;      (world-with-selected-states world (list q1 q2)))
;;;     => (list q1 q2) or (list q2 q1)
;;; STRATEGY : Use template for World on w.

;;; FUNCTION DEFINITION:
(define (world-with-selected-states w loms)
  (make-world
   (lomsg-with-selected-states (world-lomsg w) loms)
   (world-mode w)
   (world-name w)
   (world-srcstate w)
   (world-dststate w)
   (world-statecount w)
   (world-mouseprev w)))

;;; lomsg-with-selected-states : ListOfMachineStateGUI ListOfMachineState
;;;                               -> ListOfMachineStateGUI
;;; GIVEN : A lomsg and a lom.
;;; RETURNS : lomsg with the selected? updated for the lom present in lomsg.
;;; EXAMPLES :
;(lomsg-with-selected-states (world-lomsg 2STATES)
;                           (make-machine-state "\u0001" #false #false '()))  =>
;(list
; (make-machine-state-gui
;  (make-machine-state "\u0001" #false #false '())
;  150
;  50
;  #true)
; (make-machine-state-gui
;  (make-machine-state "\u0002" #true #false '())
;  50
;  50
;  #false))
;;; STRATEGY : Use HOF map on lomsg.

;;; FUNCTION DEFINITION :
(define (lomsg-with-selected-states lomsg lom)
  (map
   ; : MachineStateGUI -> MachineStateGUI.
   ; RETURNS : given msg with the selected? updated if it is present in lom.
   (lambda (msg) (update-msg-selected
                  msg
                  (member? (machine-state-gui-ms msg) lom)))
   lomsg))

;;; update-msg-selected : MachineStateGUI Boolean -> MachineStateGUI
;;; GIVEN : msg and a Boolean to denote the msg's selected? value.
;;; RETURNS : msg with selected? updated.
;;; EXAMPLES :
;(update-msg-selected (first (world-lomsg 2STATES)) #t) =>
;(make-machine-state-gui
; (make-machine-state "\u0001" #false #false '())
; 150
; 50
; #true)
;;; STRATEGY : Use template for MachineStateGUI on msg.

;;; FUNCTION DEFINITION :
(define (update-msg-selected msg value)
  (make-machine-state-gui
   (machine-state-gui-ms msg)
   (machine-state-gui-x msg)
   (machine-state-gui-y msg)
   value))

;;; machine-state-gui-color : MachineStateGUI -> String
;;; GIVEN : A MachineStateGUI
;;; RETURNS : A string representing the color that a machine state is drawn
;;; EXAMPLES :
;;;     (machine-state-gui-color machinestategui3) => START-STATE-COLOR
;;;     (machine-state-gui-color machinestategui5) => OTHER-STATES-COLOR
;;; STRATEGY : Use the template for MachineStateGUI on msg

;;; FUNCTION DEFINITION :
(define (machine-state-gui-color msg)
  (cond [(machine-state-is-start? ( machine-state-gui-ms msg))
         START-STATE-COLOR]
        [else OTHER-STATES-COLOR]))

;;; TESTS :
(begin-for-test
  (check-equal?
   (machine-state-gui-color machinestategui3)
   START-STATE-COLOR)

  (check-equal?
   (machine-state-gui-color machinestategui5)
   OTHER-STATES-COLOR)
  )

;;; machine-state-gui-circle-image : MachineStateGUI -> Image
;;; GIVEN : A MachineStateGUI
;;; RETURNS : A circle image representing a state of the given machine state
;;;     gui present in the current world state.
;;; EXAMPLES :
;;;     (machine-state-gui-circle-image machinestategui3)
;;;     => (circle OUTER-CIRCLE-RADIUS STATE-DRAWING-MODE START-STATE-COLOR)
;;;     (machine-state-gui-circle-image machinestategui5)
;;;     => (circle OUTER-CIRCLE-RADIUS STATE-DRAWING-MODE OTHER-STATES-COLOR)
;;; STRATEGY : Use the template for MachineStateGUI on msg

;;; FUNCTION DEFINITION :
(define (machine-state-gui-circle-image msg)
  (circle OUTER-CIRCLE-RADIUS STATE-DRAWING-MODE (machine-state-gui-color msg)))

;;; TESTS :
(begin-for-test
  (check-equal?
   (machine-state-gui-circle-image machinestategui3)
   (circle OUTER-CIRCLE-RADIUS STATE-DRAWING-MODE START-STATE-COLOR))

  (check-equal?
   (machine-state-gui-circle-image machinestategui5)
   (circle OUTER-CIRCLE-RADIUS STATE-DRAWING-MODE OTHER-STATES-COLOR))
  )

;;; machine-state-gui-inner-circle-image : MachineStateGUI -> Image
;;; GIVEN : A MachineStateGUI
;;; RETURNS : An inner circle image for an accepting state. If the machine
;;;     state of the given machine state gui is not accepting state, returns
;;;     an empty image.
;;; EXAMPLES :
;;;     (machine-state-gui-inner-circle-image machinestategui4) => empty-image
;;;     (machine-state-gui-inner-circle-image machinestategui5)
;;;     => (circle INNER-CIRCLE-RADIUS STATE-DRAWING-MODE OTHER-STATES-COLOR)
;;; STRATEGY : Use the template for MachineStateGUI on msg

;;; FUNCTION DEFINITION :
(define (machine-state-gui-inner-circle-image msg)
  (cond [(machine-state-is-accepting? (machine-state-gui-ms msg))
         (circle INNER-CIRCLE-RADIUS STATE-DRAWING-MODE OTHER-STATES-COLOR)]
        [else empty-image]))

;;; TESTS :
(begin-for-test
  (check-equal?
   (machine-state-gui-inner-circle-image machinestategui4)
   empty-image)

  (check-equal?
   (machine-state-gui-inner-circle-image machinestategui5)
   (circle INNER-CIRCLE-RADIUS STATE-DRAWING-MODE OTHER-STATES-COLOR))
  )
  

;;; machine-state-gui-name-for-drawing : MachineState -> String
;;; GIVEN : A MachineState
;;; RETURNS : A string representing the name of the given machine state of the
;;;     machine state gui present in the current world state. If the name
;;;     is longer than six characters, it is truncated to its first six
;;;     chracters
;;; EXAMPLES :
;;;      (machine-state-gui-name-for-drawing (machine-state-gui-ms
;;;                                              machinestategui7) => "123456"
;;; STRATEGY : Use the template for MachineState on ms

;;; FUNCTION DEFINITION :
(define (machine-state-gui-name-for-drawing ms)
  (first-six-chars (machine-state-name ms)))

;;; TESTS :
(begin-for-test
  (check-equal?
   (machine-state-gui-name-for-drawing (machine-state-gui-ms machinestategui4))
   "S1")
  
  (check-equal?
   (machine-state-gui-name-for-drawing (machine-state-gui-ms machinestategui7))
   "123456")
  )

;;; machine-state-gui-name-image : MachineStateGUI -> Image
;;; GIVEN : A MachineStateGUI
;;; RETURNS : A text image representing the name of the machine state of the
;;;     given machine state gui present in the current world state.
;;; EXAMPLES :
;;;     (machine-state-gui-name-image machinestategui4)
;;;     => (text "S1" STATE-NAME-FONT-SIZE STATE-NAME-COLOR)
;;;     (machine-state-gui-name-image machinestategui5)
;;;     => (text "S2" STATE-NAME-FONT-SIZE STATE-NAME-COLOR)
;;; STRATEGY : Use the template for MachineStateGUI on msg

;;; FUNCTION DEFINITION :
(define (machine-state-gui-name-image msg)
  (text (machine-state-gui-name-for-drawing (machine-state-gui-ms msg))
        STATE-NAME-FONT-SIZE
        STATE-NAME-COLOR))


;;; TESTS :
(begin-for-test
  (check-equal?
   (machine-state-gui-name-image machinestategui4)
   (text "S1" STATE-NAME-FONT-SIZE STATE-NAME-COLOR))
  
  (check-equal?
   (machine-state-gui-name-image machinestategui5)
   (text "S2" STATE-NAME-FONT-SIZE STATE-NAME-COLOR))
  )

;;; machine-state-gui-selected-image : MachineStateGUI -> Image
;;; GIVEN : A MachineStateGUI
;;; RETURNS : An image representing a selected machine state. If the given
;;;     machine state gui is not selected, returns an empty-image
;;; EXAMPLES :
;;;     (machine-state-gui-selected-image machinestategui4) => empty-image
;;;     (machine-state-gui-selected-image machinestategui6)
;;;     => (circle SELECTED-CIRCLE-RADIUS
;;;                SELECTED-STATE-DRAWING-MODE
;;;                SELECTED-STATE-COLOR)
;;; STRATEGY : Use the template for MachineStateGUI on msg

;;; FUNCTION DEFINITION :
(define (machine-state-gui-selected-image msg)
  (cond [(machine-state-gui-selected? msg)
         (circle SELECTED-CIRCLE-RADIUS
                 SELECTED-STATE-DRAWING-MODE
                 SELECTED-STATE-COLOR)]
        [else empty-image]))

;;; TESTS :
(begin-for-test
  (check-equal?
   (machine-state-gui-selected-image machinestategui4)
   empty-image)

  (check-equal?
   (machine-state-gui-selected-image machinestategui6)
   (circle SELECTED-CIRCLE-RADIUS
           SELECTED-STATE-DRAWING-MODE
           SELECTED-STATE-COLOR))
  )

;;; machine-state-gui-separate-from-canvas : MachineStateGUI -> Image
;;; GIVEN : A MachineStateGUI Ignored.
;;; RETURNS : An image representing a separated machine state. It is used to
;;;           apply pale-gray color to the canvas during N and T mode.
;;; EXAMPLES :
;;;     (machine-state-gui-separate-from-canvas machinestategui4) => empty-image
;;;     (machine-state-gui-separate-from-canvas machinestategui6)
;;;     => (circle SEPARATE-CIRCLE-RADIUS
;;;                SEPARATE-STATE-DRAWING-MODE
;;;                SEPARATE-STATE-COLOR)
;;; STRATEGY : Combine simpler functions.

;;; FUNCTION DEFINITION :
(define (machine-state-gui-separate-from-canvas msg)
         (circle SEPARATE-CIRCLE-RADIUS
                 SEPARATE-STATE-DRAWING-MODE
                 SEPARATE-STATE-COLOR))

;;; machine-state-gui-images : MachineStateGUI -> ListOfImage
;;; GIVEN : A MachineStateGUI
;;; RETURNS : a list of images representing a machine state present in the
;;;     current world state. 
;;; EXAMPLES :
;;;   (machine-state-gui-images machinestategui3)
;;; =>(overlay (circle OUTER-CIRCLE-RADIUS STATE-DRAWING-MODE START-STATE-COLOR)
;;;            empty-image
;;;            (text "S0" STATE-NAME-FONT-SIZE STATE-NAME-COLOR)
;;;            empty-image)
;;;  (machine-state-gui-images machinestategui6)
;;;=>(overlay (circle OUTER-CIRCLE-RADIUS STATE-DRAWING-MODE OTHER-STATES-COLOR)
;;;           (circle INNER-CIRCLE-RADIUS STATE-DRAWING-MODE OTHER-STATES-COLOR)
;;;           (text "S3" STATE-NAME-FONT-SIZE STATE-NAME-COLOR)
;;;           (circle SELECTED-CIRCLE-RADIUS SELECTED-STATE-DRAWING-MODE
;;;                   SELECTED-STATE-COLOR))
;;; STRATEGY : Combine Simpler functions

;;; FUNCTION DEFINITION :
(define (machine-state-gui-images msg)
  (overlay (machine-state-gui-circle-image msg)
           (machine-state-gui-inner-circle-image msg)
           (machine-state-gui-name-image msg)
           (machine-state-gui-selected-image msg)
           (machine-state-gui-separate-from-canvas msg)))

;;; TESTS :
(begin-for-test
  (check-equal?
   (machine-state-gui-images machinestategui3)
   (overlay (circle OUTER-CIRCLE-RADIUS STATE-DRAWING-MODE START-STATE-COLOR)
            empty-image
            (text "S0" STATE-NAME-FONT-SIZE STATE-NAME-COLOR)
            empty-image
            (circle SEPARATE-CIRCLE-RADIUS SEPARATE-STATE-DRAWING-MODE
                     SEPARATE-STATE-COLOR)))
  
  (check-equal?
   (machine-state-gui-images machinestategui6)
   (overlay (circle OUTER-CIRCLE-RADIUS STATE-DRAWING-MODE OTHER-STATES-COLOR)
            (circle INNER-CIRCLE-RADIUS STATE-DRAWING-MODE OTHER-STATES-COLOR)
            (text "S3" STATE-NAME-FONT-SIZE STATE-NAME-COLOR)
            (circle SELECTED-CIRCLE-RADIUS SELECTED-STATE-DRAWING-MODE
                    SELECTED-STATE-COLOR)
            (circle SEPARATE-CIRCLE-RADIUS SEPARATE-STATE-DRAWING-MODE
                     SEPARATE-STATE-COLOR)))
  )


;;; lomsg-images : ListOfMachineStateGUI -> ListOfImage
;;; GIVEN : A list of MachineStateGUIs present in the current world state.
;;; RETURNS : a list of images representing machine state guis present in the
;;;     current world state. 
;;; EXAMPLES :
;;;   => (lomsg-images (list machinestategui3 machinestategui6))
;;;      (list
;;;       (overlay
;;;         (circle OUTER-CIRCLE-RADIUS STATE-DRAWING-MODE START-STATE-COLOR)
;;;         empty-image
;;;         (text "S0" STATE-NAME-FONT-SIZE STATE-NAME-COLOR)
;;;         empty-image)
;;;       (overlay
;;;        (circle OUTER-CIRCLE-RADIUS STATE-DRAWING-MODE OTHER-STATES-COLOR)
;;;        (circle INNER-CIRCLE-RADIUS STATE-DRAWING-MODE OTHER-STATES-COLOR)
;;;        (text "S3" STATE-NAME-FONT-SIZE STATE-NAME-COLOR)
;;;        (circle SELECTED-CIRCLE-RADIUS SELECTED-STATE-DRAWING-MODE
;;;                SELECTED-STATE-COLOR)))
;;; STRATEGY : Use HOF map for ListOfMachineStateGUI on lomsg

;;; FUNCTION DEFINITION :
(define (lomsg-images lomsg)
  (map
   machine-state-gui-images
   lomsg))

;;; TESTS :
(begin-for-test
  (check-equal?
   (lomsg-images (list machinestategui3 machinestategui6))
   (list
    (overlay (circle OUTER-CIRCLE-RADIUS STATE-DRAWING-MODE START-STATE-COLOR)
             empty-image
             (text "S0" STATE-NAME-FONT-SIZE STATE-NAME-COLOR)
             empty-image
             (circle SEPARATE-CIRCLE-RADIUS SEPARATE-STATE-DRAWING-MODE
                     SEPARATE-STATE-COLOR))
    
    (overlay (circle OUTER-CIRCLE-RADIUS STATE-DRAWING-MODE OTHER-STATES-COLOR)
             (circle INNER-CIRCLE-RADIUS STATE-DRAWING-MODE OTHER-STATES-COLOR)
             (text "S3" STATE-NAME-FONT-SIZE STATE-NAME-COLOR)
             (circle SELECTED-CIRCLE-RADIUS SELECTED-STATE-DRAWING-MODE
                     SELECTED-STATE-COLOR)
             (circle SEPARATE-CIRCLE-RADIUS SEPARATE-STATE-DRAWING-MODE
                     SEPARATE-STATE-COLOR))))
  )

;;; machine-state-gui-image-posn : MachineStateGUI -> POSN
;;; GIVEN : A MachineStateGUI
;;; RETURNS : A posn representing the x, y coordinates where images for the
;;;     machine state of the given machine state gui is drawn.
;;; EXAMPLES :
;;;     (machine-state-gui-image-posn machinestategui4) => (make-posn 40 40)
;;;     (machine-state-gui-image-posn machinestategui6) => (make-posn 60 20)
;;; STRATEGY : Use the template for MachineStateGUI on msg

;;; FUNCTION DEFINITION :
(define (machine-state-gui-image-posn msg)
  (make-posn (machine-state-gui-x msg)
             (machine-state-gui-y msg))) 

;;; TESTS :
(begin-for-test
  (check-equal?
   (machine-state-gui-image-posn machinestategui4) (make-posn 40 40))
  (check-equal?
   (machine-state-gui-image-posn machinestategui6) (make-posn 60 20))
   )

;;; lomsg-image-posns : ListOfMachineStateGUI -> ListOfPOSN
;;; GIVEN : A list of MachineStateGUIs present in the current world state.
;;; RETURNS : a list of POSNs representing the position of machine state guis
;;;     present in the current world state. 
;;; EXAMPLES :
;;;  (lomsg-image-posns machinestateguilist4)
;;;   (list (make-posn 20 20) (make-posn 40 40) (make-posn 60 60))
;;;  (lomsg-image-posns (list machinestategui6))
;;;   (list (make-posn 60 20))
;;; STRATEGY : Use HOF map for ListOfMachineStateGUI on lomsg

;;; FUNCTION DEFINITION :
(define (lomsg-image-posns lomsg)
  (map
   machine-state-gui-image-posn
   lomsg))

;;; TESTS :
(begin-for-test
  (check-equal?
   (lomsg-image-posns machinestateguilist4)
   (list (make-posn 20 20) (make-posn 40 40) (make-posn 60 60)))
  
  (check-equal?
   (lomsg-image-posns (list machinestategui6))
   (list (make-posn 60 20)))
  )
   

;;; machine-state-gui-transition-curve-start-x :
;;;     MachineStateGUI MachineStateGUI -> Integer
;;; GIVEN : Two MachineStateGUIs that have a transition in between
;;; RETURNS : An x coordinate where a curve representing a transition starts
;;; EXAMPLES :
;;;   (machine-state-gui-transition-curve-start-x
;;;    machinestategui3 machinestategui3)
;;;   => (+ 20 TRANSITION-CURVE-TO-ITSELF-START-X-OFFSET)
;;;   (machine-state-gui-transition-curve-start-x
;;;    machinestategui3 machinestategui4)
;;;   => (+ 20 OUTER-CIRCLE-RADIUS)
;;; STRATEGY : Use the template for MachineStateGUI
;;; FUNCTION DEFINITION : 
(define (machine-state-gui-transition-curve-start-x src dest)
  (cond [(equal? src dest)
         (+ (machine-state-gui-x src)
            TRANSITION-CURVE-TO-ITSELF-START-X-OFFSET)]
        [else (+ (machine-state-gui-x src) OUTER-CIRCLE-RADIUS)]))


;;; TESTS :
(begin-for-test
  (check-equal?
   (machine-state-gui-transition-curve-start-x
    machinestategui3 machinestategui3)
   (+ 20 TRANSITION-CURVE-TO-ITSELF-START-X-OFFSET))
  
  (check-equal?
   (machine-state-gui-transition-curve-start-x
    machinestategui3 machinestategui4)
   (+ 20 OUTER-CIRCLE-RADIUS))
  )

;;; machine-state-gui-transition-curve-start-y :
;;;     MachineStateGUI MachineStateGUI -> Integer
;;; GIVEN : Two MachineStateGUIs that have a transition in between
;;; RETURNS : An y coordinate where a curve representing a transition starts
;;; EXAMPLES :
;;;   (machine-state-gui-transition-curve-start-y
;;;    machinestategui3 machinestategui3)
;;;   => (+ 20 TRANSITION-CURVE-TO-ITSELF-START-Y-OFFSET)
;;;   (machine-state-gui-transition-curve-start-y
;;;    machinestategui3 machinestategui4)
;;;   => 20
;;; STRATEGY : Use the template for MachineStateGUI
;;; FUNCTION DEFINITION :
(define (machine-state-gui-transition-curve-start-y src dest)
  (cond [(equal? src dest)
         (+ (machine-state-gui-y src)
            TRANSITION-CURVE-TO-ITSELF-START-Y-OFFSET)]
        [else (machine-state-gui-y src)]))


;;; TESTS :
(begin-for-test
  (check-equal?
   (machine-state-gui-transition-curve-start-y
    machinestategui3 machinestategui3)
   (+ 20 TRANSITION-CURVE-TO-ITSELF-START-Y-OFFSET))
  
  (check-equal?
   (machine-state-gui-transition-curve-start-y
    machinestategui3 machinestategui4)
   20)
  )


;;; machine-state-gui-transition-curve-end-x :
;;;     MachineStateGUI MachineStateGUI -> Integer
;;; GIVEN : Two MachineStateGUIs that have a transition in between
;;; RETURNS : An x coordinate where a curve representing a transition ends
;;; EXAMPLES :
;;;   (machine-state-gui-transition-curve-end-x
;;;    machinestategui3 machinestategui3)
;;;   => (+ 20 TRANSITION-CURVE-TO-ITSELF-END-X-OFFSET)
;;;   (machine-state-gui-transition-curve-end-x
;;;    machinestategui3 machinestategui4)
;;;   => (- 40 OUTER-CIRCLE-RADIUS)
;;;   (machine-state-gui-transition-curve-end-x
;;;    machinestategui4 machinestategui3)
;;;   => 20
;;; STRATEGY : Use the template for MachineStateGUI
   
;;; FUNCTION DEFINITION :
(define (machine-state-gui-transition-curve-end-x src dest)
  (cond [(equal? src dest)
         (+ (machine-state-gui-x dest) TRANSITION-CURVE-TO-ITSELF-END-X-OFFSET)]
        [(< (machine-state-gui-x src) (machine-state-gui-x dest))
         (- (machine-state-gui-x dest) OUTER-CIRCLE-RADIUS)]
        [else (machine-state-gui-x dest)]))

;;; TESTS :
(begin-for-test
  (check-equal?
   (machine-state-gui-transition-curve-end-x
    machinestategui3 machinestategui3)
   (+ 20 TRANSITION-CURVE-TO-ITSELF-END-X-OFFSET))
  
  (check-equal?
   (machine-state-gui-transition-curve-end-x
    machinestategui3 machinestategui4)
   (- 40 OUTER-CIRCLE-RADIUS))
  
  (check-equal?
   (machine-state-gui-transition-curve-end-x
    machinestategui4 machinestategui3)
   20)
  )

;;; machine-state-gui-transition-curve-end-y :
;;;     MachineStateGUI MachineStateGUI -> Integer
;;; GIVEN : Two MachineStateGUIs that have a transition in between
;;; RETURNS : An y coordinate where a curve representing a transition ends
;;; EXAMPLES :
;;;   (machine-state-gui-transition-curve-end-y
;;;    machinestategui3 machinestategui3)
;;;   => (+ 20 TRANSITION-CURVE-TO-ITSELF-END-Y-OFFSET)
;;;   (machine-state-gui-transition-curve-end-y
;;;    machinestategui3 machinestategui4)
;;;   => 40
;;;  (machine-state-gui-transition-curve-end-y
;;;    machinestategui4 machinestategui3)
;;;   => (+ 20 OUTER-CIRCLE-RADIUS)
;;;  (machine-state-gui-transition-curve-end-y
;;;    machinestategui6 machinestategui4)
;;;   => (- 40 OUTER-CIRCLE-RADIUS)
;;; STRATEGY : Use the template for MachineStateGUI

;;; FUNCTION DEFINITION :
(define (machine-state-gui-transition-curve-end-y src dest)
  (cond [(equal? src dest)
         (+ (machine-state-gui-y dest) TRANSITION-CURVE-TO-ITSELF-END-Y-OFFSET)]
        [(< (machine-state-gui-x src) (machine-state-gui-x dest))
         (machine-state-gui-y dest)]
        [(< (machine-state-gui-y src) (machine-state-gui-y dest))
         (- (machine-state-gui-y dest) OUTER-CIRCLE-RADIUS)]
        [else (+ (machine-state-gui-y dest) OUTER-CIRCLE-RADIUS)]))

;;; TESTS :
(begin-for-test
  (check-equal?
   (machine-state-gui-transition-curve-end-y
    machinestategui3 machinestategui3)
   (+ 20 TRANSITION-CURVE-TO-ITSELF-END-Y-OFFSET))
  
  (check-equal?
   (machine-state-gui-transition-curve-end-y
    machinestategui3 machinestategui4)
   40)
  
  (check-equal?
   (machine-state-gui-transition-curve-end-y
    machinestategui4 machinestategui3)
   (+ 20 OUTER-CIRCLE-RADIUS ))
  
  (check-equal?
   (machine-state-gui-transition-curve-end-y
    machinestategui6 machinestategui4)
   (- 40 OUTER-CIRCLE-RADIUS ))
  )

;;; machine-state-gui-transition-curve-start-angle :
;;;     MachineStateGUI MachineStateGUI -> Real
;;; GIVEN : Two MachineStateGUIs that have a transition in between
;;; RETURNS : The initial angle of the curve for the transition.
;;; EXAMPLES :
;;;   (machine-state-gui-transition-curve-start-angle
;;;    machinestategui3 machinestategui3)
;;;   => TRANSITION-CURVE-TO-ITSELF-START-ANGLE
;;;   (machine-state-gui-transition-curve-start-angle
;;;    machinestategui3 machinestategui4)
;;;   => TRANSITION-CURVE-START-ANGLE-DOWN
;;;   (machine-state-gui-transition-curve-start-angle
;;;    machinestategui5 machinestategui3)
;;;   => TRANSITION-CURVE-START-ANGLE-UP
;;; STRATEGY : Use the template for MachineStateGUI

;;; FUNCTION DEFINI&TION :
(define (machine-state-gui-transition-curve-start-angle src dest)
  (cond [(equal? src dest) TRANSITION-CURVE-TO-ITSELF-START-ANGLE]
        [(< (machine-state-gui-y src) (machine-state-gui-y dest))
         TRANSITION-CURVE-START-ANGLE-DOWN]
        [else TRANSITION-CURVE-START-ANGLE-UP]))

;;; TESTS :
(begin-for-test
  (check-equal?
   (machine-state-gui-transition-curve-start-angle
    machinestategui3 machinestategui3)
   TRANSITION-CURVE-TO-ITSELF-START-ANGLE)
  
  (check-equal?
   (machine-state-gui-transition-curve-start-angle
    machinestategui3 machinestategui4)
   TRANSITION-CURVE-START-ANGLE-DOWN)
  
  (check-equal?
   (machine-state-gui-transition-curve-start-angle
    machinestategui5 machinestategui3)
   TRANSITION-CURVE-START-ANGLE-UP)
  )

;;; machine-state-gui-transition-curve-start-pull :
;;;     MachineStateGUI MachineStateGUI -> integer
;;; GIVEN : Two MachineStateGUIs that have a transition in between
;;; RETURNS : a real number representing how long a curve tries to stay with
;;;    the initial angle of it.
;;; EXAMPLES :
;;;   (machine-state-gui-transition-curve-start-pull
;;;    machinestategui3 machinestategui3)
;;;   => TRANSITION-CURVE-TO-ITSELF-START-PULL)
;;;   (machine-state-gui-transition-curve-start-pull
;;;    machinestategui3 machinestategui4)
;;;   => TRANSITION-CURVE-START-PULL)
;;; STRATEGY : Use the template for MachineStateGUI

;;; FUNCTION DEFINITION :
(define (machine-state-gui-transition-curve-start-pull src dest)
  (cond [(equal? src dest) TRANSITION-CURVE-TO-ITSELF-START-PULL]
        [else TRANSITION-CURVE-START-PULL]))

;;; TESTS :
(begin-for-test
  (check-equal?
   (machine-state-gui-transition-curve-start-pull
    machinestategui3 machinestategui3)
   TRANSITION-CURVE-TO-ITSELF-START-PULL)
  
  (check-equal?
   (machine-state-gui-transition-curve-start-pull
    machinestategui3 machinestategui4)
   TRANSITION-CURVE-START-PULL)
  )
    
;;; machine-state-gui-transition-curve-end-angle :
;;;     MachineStateGUI MachineStateGUI -> Real
;;; GIVEN : Two MachineStateGUIs that have a transition in between
;;; RETURNS : The end angle of the curve for the transition
;;; EXAMPLES :
;;;     (machine-state-gui-transition-curve-end-angle
;;;      machinestategui3 machinestategui3)
;;;     => TRANSITION-CURVE-TO-ITSELF-END-ANGLE
;;;     (machine-state-gui-transition-curve-end-angle
;;;      machinestategui3 machinestategui4)
;;;     => TRANSITION-CURVE-END-ANGLE-RIGHT-DOWN
;;;     (machine-state-gui-transition-curve-end-angle
;;;      machinestategui3 machinestategui6)
;;;     => TRANSITION-CURVE-END-ANGLE-RIGHT-UP
;;;     (machine-state-gui-transition-curve-end-angle
;;;      machinestategui4 machinestategui3)
;;;     => TRANSITION-CURVE-END-ANGLE-LEFT-UP
;;;     (machine-state-gui-transition-curve-end-angle
;;;       machinestategui6 machinestategui4)
;;;     => TRANSITION-CURVE-END-ANGLE-LEFT-DOWN
;;; STRATEGY : Use the template for MachineStateGUI
;;; FUNCTION DEFINITION :
(define (machine-state-gui-transition-curve-end-angle src dest)
  (cond [(equal? src dest) TRANSITION-CURVE-TO-ITSELF-END-ANGLE]
        [(and (< (machine-state-gui-x src) (machine-state-gui-x dest))
              (< (machine-state-gui-y src) (machine-state-gui-y dest)))
         TRANSITION-CURVE-END-ANGLE-RIGHT-DOWN]
        [(and (< (machine-state-gui-x src) (machine-state-gui-x dest))
              (>= (machine-state-gui-y src) (machine-state-gui-y dest)))
         TRANSITION-CURVE-END-ANGLE-RIGHT-UP]
        [(< (machine-state-gui-y src) (machine-state-gui-y dest))
         TRANSITION-CURVE-END-ANGLE-LEFT-DOWN]
        [else TRANSITION-CURVE-END-ANGLE-LEFT-UP]))

;;; TESTS :
(begin-for-test
  (check-equal?
   (machine-state-gui-transition-curve-end-angle
    machinestategui3 machinestategui3)
   TRANSITION-CURVE-TO-ITSELF-END-ANGLE)
  
  (check-equal?
   (machine-state-gui-transition-curve-end-angle
    machinestategui3 machinestategui4)
   TRANSITION-CURVE-END-ANGLE-RIGHT-DOWN)
  
  (check-equal?
   (machine-state-gui-transition-curve-end-angle
    machinestategui3 machinestategui6)
   TRANSITION-CURVE-END-ANGLE-RIGHT-UP)
  
  (check-equal?
   (machine-state-gui-transition-curve-end-angle
    machinestategui4 machinestategui3)
   TRANSITION-CURVE-END-ANGLE-LEFT-UP)
  
  (check-equal?
   (machine-state-gui-transition-curve-end-angle
    machinestategui6 machinestategui4)
   TRANSITION-CURVE-END-ANGLE-LEFT-DOWN)
  )


;;; machine-state-gui-transition-curve-end-pull :
;;;     MachineStateGUI MachineStateGUI -> Real
;;; GIVEN : Two MachineStateGUIs that have a transition in between
;;; RETURNS : a real number representing how long the curve tries to stay with
;;;    the end angle.
;;; EXAMPLES :
;;;   (machine-state-gui-transition-curve-end-pull
;;;    machinestategui3 machinestategui3)
;;;   => TRANSITION-CURVE-TO-ITSELF-END-PULL
;;;   (machine-state-gui-transition-curve-end-pull
;;;    machinestategui3 machinestategui4)
;;;   => TRANSITION-CURVE-END-PULL
;;; STRATEGY : Use the template for MachineStateGUI

;;; FUNCTION DEFINITION :
(define (machine-state-gui-transition-curve-end-pull src dest)
  (cond [(equal? src dest) TRANSITION-CURVE-TO-ITSELF-END-PULL]
        [else TRANSITION-CURVE-END-PULL]))

;;; TESTS :
(begin-for-test
  (check-equal?
   (machine-state-gui-transition-curve-end-pull
    machinestategui3 machinestategui3)
   TRANSITION-CURVE-TO-ITSELF-END-PULL)
  
  (check-equal?
   (machine-state-gui-transition-curve-end-pull
    machinestategui3 machinestategui4)
   TRANSITION-CURVE-END-PULL)
  )

;;; lomsg-transition-curves : ListOfMachineStateGUI Image -> Image
;;; GIVEN : A list of MachineStateGUIs present in the current world state and
;;;     a background scene image where curves and arrows are drawn.
;;; RETURNS : An image representing the transitions of the machine states
;;;     present in the current world state. 
;;; EXAMPLES :
;;;   (lomsg-transition-curves guilist1 CANVAS)
;;;   => (place-image
;;;       (machine-state-gui-transition-curve-arrow gui1 gui2)
;;;       (- 40 OUTER-CIRCLE-RADIUS) 40
;;;       (machine-state-gui-transition-curve
;;;        gui1 gui2
;;;        (place-image
;;;         (machine-state-gui-transition-curve-arrow gui1 gui1)
;;;         (+ 20 TRANSITION-CURVE-TO-ITSELF-END-X-OFFSET)
;;;         (+ 20 TRANSITION-CURVE-TO-ITSELF-END-Y-OFFSET)
;;;         (machine-state-gui-transition-curve gui1 gui1 CANVAS)))
;;; STRATEGY : Use HOF map for ListOfMachineStateGUI on lomsg

;;; FUNCTION DEFINITION :
(define (lomsg-transition-curves lomsg bg)
  (foldr
   ;;; MachineStateGUI Image -> Image
   ;;; GIVEN : A MachineStateGUI present in the current world state and
   ;;;     a background scene image where curves and arrows are drawn.
   ;;; RETURNS : An image cotains curves and arrows representing the transitions
   ;;;     of the given machine state gui. 
   (lambda (msg scene)
     (machine-state-gui-transition-curves-with-arrow msg lomsg scene))
   bg
   lomsg))

;;; TESTS :
(begin-for-test
  (check-equal?
   (lomsg-transition-curves guilist1 CANVAS)
   (place-image
    (machine-state-gui-transition-curve-arrow gui1 gui2)
    (- 40 OUTER-CIRCLE-RADIUS) 40
    (machine-state-gui-transition-curve
     gui1 gui2
     (place-image
      (machine-state-gui-transition-curve-arrow gui1 gui1)
      (+ 20 TRANSITION-CURVE-TO-ITSELF-END-X-OFFSET)
      (+ 20 TRANSITION-CURVE-TO-ITSELF-END-Y-OFFSET)
      (machine-state-gui-transition-curve gui1 gui1 CANVAS)))))
   )
  


;;; machine-state-gui-transition-curve-arrow :
;;;     MachineStateGUI MachineStateGUI Image -> Image
;;; GIVEN : Two MachineStateGUIs that have a transition in between
;;; RETURNS : An arrow image for the transition.
;;; EXAMPLES :
;;;    (machine-state-gui-transition-curve-arrow
;;;     machinestategui3 machinestategui3)
;;;    => (rotate
;;;        (machine-state-gui-transition-curve-end-angle
;;;         machinestategui3 machinestategui3)
;;;        (text TRANSITION-ARROW-SYMBOL TRANSITION-ARROW-SIZE
;;;              TRANSITION-ARROW-COLOR))
;;;    (machine-state-gui-transition-curve-arrow
;;;     machinestategui3 machinestategui4)
;;;    => (rotate
;;;        (machine-state-gui-transition-curve-end-angle
;;;         machinestategui3 machinestategui4)
;;;        (text TRANSITION-ARROW-SYMBOL TRANSITION-ARROW-SIZE
;;;              TRANSITION-ARROW-COLOR))
;;; STRATEGY : Use the template for MachineStateGUI on msg

;;; FUNCTION DEFINITION :
(define (machine-state-gui-transition-curve-arrow src dest)
  (rotate
   (machine-state-gui-transition-curve-end-angle src dest)
   (text TRANSITION-ARROW-SYMBOL TRANSITION-ARROW-SIZE TRANSITION-ARROW-COLOR)))

;;; TESTS :
(begin-for-test
  (check-equal?
   (machine-state-gui-transition-curve-arrow
    machinestategui3 machinestategui3)
   (rotate
    (machine-state-gui-transition-curve-end-angle
     machinestategui3 machinestategui3)
    (text TRANSITION-ARROW-SYMBOL TRANSITION-ARROW-SIZE
          TRANSITION-ARROW-COLOR)))
  
  (check-equal?
   (machine-state-gui-transition-curve-arrow
    machinestategui3 machinestategui4)
   (rotate
    (machine-state-gui-transition-curve-end-angle
     machinestategui3 machinestategui4)
    (text TRANSITION-ARROW-SYMBOL TRANSITION-ARROW-SIZE
          TRANSITION-ARROW-COLOR)))
  )

;;; machine-state-gui-transition-curve :
;;;     MachineStateGUI MachineStateGUI Image -> Image
;;; GIVEN : Two MachineStateGUIs that have a transition in between and
;;;     a scene image where a curve is drawn.
;;; RETURNS : An image representing the transition between the given
;;;      MachineStateGUIs
;;; EXAMPLES :
;;;   (machine-state-gui-transition-curve gui1 gui2 CANVAS)
;;;   => (scene+curve
;;;       CANVAS
;;;       (+ (machine-state-gui-x gui1) OUTER-CIRCLE-RADIUS)
;;;       (machine-state-gui-y gui1)
;;;       TRANSITION-CURVE-START-ANGLE-DOWN
;;;       TRANSITION-CURVE-START-PULL
;;;       (- (machine-state-gui-x gui2) OUTER-CIRCLE-RADIUS)
;;;       (machine-state-gui-y gui2)
;;;       TRANSITION-CURVE-END-ANGLE-RIGHT-DOWN
;;;       TRANSITION-CURVE-END-PULL
;;;       TRANSITION-CURVE-COLOR)
;;; STRATEGY : Combine Simpler functions

;; FUNCTION DEFINITION :
(define (machine-state-gui-transition-curve src dest bg)
  (scene+curve
   bg
   (machine-state-gui-transition-curve-start-x src dest)
   (machine-state-gui-transition-curve-start-y src dest)
   (machine-state-gui-transition-curve-start-angle src dest)
   (machine-state-gui-transition-curve-start-pull src dest)
   (machine-state-gui-transition-curve-end-x src dest)
   (machine-state-gui-transition-curve-end-y src dest)
   (machine-state-gui-transition-curve-end-angle src dest)
   (machine-state-gui-transition-curve-end-pull src dest)
   TRANSITION-CURVE-COLOR))

;;; TESTS :
(begin-for-test
  (check-equal?
   (machine-state-gui-transition-curve gui1 gui2 CANVAS)
   (scene+curve
    CANVAS
    (+ (machine-state-gui-x gui1) OUTER-CIRCLE-RADIUS)
    (machine-state-gui-y gui1)
    TRANSITION-CURVE-START-ANGLE-DOWN
    TRANSITION-CURVE-START-PULL
    (- (machine-state-gui-x gui2) OUTER-CIRCLE-RADIUS)
    (machine-state-gui-y gui2)
    TRANSITION-CURVE-END-ANGLE-RIGHT-DOWN
    TRANSITION-CURVE-END-PULL
    TRANSITION-CURVE-COLOR))
  )

;;; machine-state-gui-transition-curves-with-arrow :
;;;     MachineStateGUI ListOfMachineStateGUI Image -> Image
;;; GIVEN : A MachineStateGUI
;;; RETURNS : An image representing transitions of the machine state of the
;;;     given machine state gui.
;;; EXAMPLES :
;;;   (machine-state-gui-transition-curves-with-arrow
;;;    gui1 machinestateguilist4 CANVAS)
;;;   => (place-image
;;;       (machine-state-gui-transition-curve-arrow gui1 gui2)
;;;       (- 40 OUTER-CIRCLE-RADIUS) 40
;;;       (machine-state-gui-transition-curve
;;;        gui1 gui2
;;;        (place-image
;;;         (machine-state-gui-transition-curve-arrow gui1 gui1)
;;;         (+ 20 TRANSITION-CURVE-TO-ITSELF-END-X-OFFSET)
;;;         (+ 20 TRANSITION-CURVE-TO-ITSELF-END-Y-OFFSET)
;;;         (machine-state-gui-transition-curve gui1 gui1 CANVAS))))
;;; STRATEGY : Use HOF foldr for ListOfMachineStateGUI
;;;     on (lot-to-lomsg (machine-state-gui-transitions msg) lomsg)

;;; FUNCTION DEFINITION :
(define (machine-state-gui-transition-curves-with-arrow msg lomsg bg)
  (foldr
   ;;; MachineStateGUI Image -> Image
   ;;; GIVEN : A MachineStateGUI, which is the destnation of a transition from
   ;;;     the given MachineStateGUI, msg.
   ;;; RETURNS :  An image that contains curves with arrows representing
   ;;;    transitions to the machine state of the given machine state gui from
   ;;;    msg.
   (lambda (msgdest scene)
     (place-image
      (machine-state-gui-transition-curve-arrow msg msgdest)
      (machine-state-gui-transition-curve-end-x msg msgdest)
      (machine-state-gui-transition-curve-end-y msg msgdest)
      (machine-state-gui-transition-curve msg msgdest scene)))
   bg
   (lot-to-lomsg (machine-state-gui-transitions msg) lomsg)))
 
;;; TESTS :
(begin-for-test
  (check-equal?
   (machine-state-gui-transition-curves-with-arrow
    gui1 machinestateguilist4 CANVAS)
   (place-image
    (machine-state-gui-transition-curve-arrow gui1 gui2)
    (- 40 OUTER-CIRCLE-RADIUS) 40
    (machine-state-gui-transition-curve
     gui1 gui2
     (place-image
      (machine-state-gui-transition-curve-arrow gui1 gui1)
      (+ 20 TRANSITION-CURVE-TO-ITSELF-END-X-OFFSET)
      (+ 20 TRANSITION-CURVE-TO-ITSELF-END-Y-OFFSET)
      (machine-state-gui-transition-curve gui1 gui1 CANVAS)))))
   )
  
  
 
;;; lomsg-state-gui-transition-input-symbol-image-posns :
;;;     ListOfMachineStateGUI -> ListOfPOSN
;;; GIVEN : A list of MachineStateGUIs present in the current world
;;; RETURNS : A list of POSNs representing where the input symbols of the
;;;     transitions are drawn.
;;; EXAMPLES :
;;;   (lomsg-state-gui-transition-input-symbol-image-posns machinestateguilist4)
;;;   => (list (make-posn (+ 20 INPUT-SYMBOL-X-POS-OFFSET)
;;;                       (+ 20 INPUT-SYMBOL-Y-POS-OFFSET))
;;;            (make-posn (+ 20 14) (+ 20 14))
;;;            (make-posn (+ 40 INPUT-SYMBOL-X-POS-OFFSET)
;;;                       (+ 40 INPUT-SYMBOL-Y-POS-OFFSET))
;;;            (make-posn (+ 40 14) (+ 40 14))
;;;            (make-posn (- 60 28) (- 60 28))))
;;; STRATEGY : Use HOF fold for ListOfMachineStateGUI on lomsg

;;; FUNCTION DEFINITION :
(define (lomsg-state-gui-transition-input-symbol-image-posns lomsg)
  (foldr
   ;;; MachineStateGUI ListOfPOSN -> ListOfPOSN
   ;;; GIVEN : A MachineStateGUI present in the current world and a base list of
   ;;;     POSNs
   ;;; RETURNS : A list of POSNs representing where the input symbols of the
   ;;;     transitions are drawn.
   (lambda (msg r)
     (append
      (machine-state-gui-transition-input-symbol-image-posns msg lomsg) r))
   empty
   lomsg))

;;; TESTS :
(begin-for-test
  (check-equal?
   (lomsg-state-gui-transition-input-symbol-image-posns machinestateguilist4)
   (list (make-posn (+ 20 INPUT-SYMBOL-X-POS-OFFSET)
                    (+ 20 INPUT-SYMBOL-Y-POS-OFFSET))
         (make-posn (+ 20 14) (+ 20 14))
         (make-posn (+ 40 INPUT-SYMBOL-X-POS-OFFSET)
                    (+ 40 INPUT-SYMBOL-Y-POS-OFFSET))
         (make-posn (+ 40 14) (+ 40 14))
         (make-posn (- 60 28) (- 60 28))))
  )

;;; machine-state-gui-transition-input-symbol-image-posns :
;;;     MachineStateGUI ListOfMachineStateGUI -> ListOfPOSN
;;; GIVEN :  A MachineStateGUI and a list of MachinesStateGUIs in the current
;;;     world state.
;;; RETURNS : A list of POSNs representing where input symbols of the
;;;     given transitions are drawn.
;;; EXAMPLES :
;;;   (machine-state-gui-transition-input-symbol-image-posns
;;;    machinestategui3 machinestateguilist4)
;;;   => (list (make-posn (+ 20 INPUT-SYMBOL-X-POS-OFFSET)
;;;                    (+ 20 INPUT-SYMBOL-Y-POS-OFFSET))
;;;            (make-posn (+ 20 14) (+ 20 14))))
;;;   (machine-state-gui-transition-input-symbol-image-posns
;;;     machinestategui5 machinestateguilist4)
;;;    =>(list (make-posn (- 60 28) (- 60 28)))
;;; STRATEGY : Use HOF foldr for ListOfMachineStateGUI
;;;      on (lot-to-lomsg (machine-state-gui-transitions msg) lomsg))

;;; FUNCTION DEFINITION :
(define (machine-state-gui-transition-input-symbol-image-posns msg lomsg)
  (foldr
   ;;; MachineStateGUI ListOfPOSN -> ListOfPOSN
   ;;; GIVEN : The destination machine state gui of a transition
   ;;; RETURNS :  A list of POSNs representing where the input symbol of the
   ;;;     transition to the given machine state gui is drawn.
   (lambda (msgdest r)
     (cons
      (make-posn (machine-state-gui-transition-input-symbol-x msg msgdest) 
                 (machine-state-gui-transition-input-symbol-y msg msgdest)) r))
   empty
   (lot-to-lomsg (machine-state-gui-transitions msg) lomsg)))

;;; TESTS :
(begin-for-test
  (check-equal?
   (machine-state-gui-transition-input-symbol-image-posns
    machinestategui3 machinestateguilist4)
   (list (make-posn (+ 20 INPUT-SYMBOL-X-POS-OFFSET)
                    (+ 20 INPUT-SYMBOL-Y-POS-OFFSET))
         (make-posn (+ 20 14) (+ 20 14))))
  
  (check-equal?
   (machine-state-gui-transition-input-symbol-image-posns
    machinestategui5 machinestateguilist4)
   (list (make-posn (- 60 28) (- 60 28))))
  )

;;; machine-state-gui-transition-input-symbol-x :
;;;     MachineStateGUI MachineStateGUI -> Integer
;;; GIVEN : Two MachineStateGUIs that have a transition in between
;;; RETURNS : An x coordinate where the input symbol of that transition is drawn
;;; EXAMPLES :
;;;   (machine-state-gui-transition-input-symbol-x
;;;    (make-machine-state-gui
;;;     (make-machine-state
;;;      "S0" true false
;;;      (list (make-transition "A" "S0") (make-transition "B" "S1"))) 
;;;     20 20 false)
;;;    (make-machine-state-gui
;;;     (make-machine-state
;;;      "S1" true false
;;;      (list (make-transition "B" "S1") (make-transition "C" "S2"))) 
;;;     40 40 false))
;;;   => (+ 20 14))
;;;   (machine-state-gui-transition-input-symbol-x
;;;    (make-machine-state-gui
;;;     (make-machine-state
;;;      "S0" true false
;;;      (list (make-transition "A" "S0") (make-transition "B" "S1"))) 
;;;     20 20 false)
;;;    (make-machine-state-gui
;;;     (make-machine-state
;;;      "S0" true false
;;;      (list (make-transition "A" "S0") (make-transition "B" "S1"))) 
;;;     20 20 false))
;;;   => (+ 20 INPUT-SYMBOL-Y-POS-OFFSET))
;;; STRATEGY : Use the template for MachineStateGUI

;;; FUNCTION DEFINITION :
(define (machine-state-gui-transition-input-symbol-x src dest)
  (+ (machine-state-gui-x src)
     (cond [(equal? src dest) INPUT-SYMBOL-X-POS-OFFSET]
           [else
            (* (- (machine-state-gui-x dest)
                  (machine-state-gui-x src))
               INPUT-SYMBOL-X-POS-WEIGHT)])))

;;; TESTS :
(begin-for-test
  (check-equal?
   (machine-state-gui-transition-input-symbol-x
    (make-machine-state-gui
     (make-machine-state
      "S0" true false
      (list (make-transition "A" "S0") (make-transition "B" "S1"))) 
     20 20 false)
    (make-machine-state-gui
     (make-machine-state
      "S1" true false
      (list (make-transition "B" "S1") (make-transition "C" "S2"))) 
     40 40 false))
   (+ 20 14))
  
  (check-equal?
   (machine-state-gui-transition-input-symbol-x
    (make-machine-state-gui
     (make-machine-state
      "S0" true false
      (list (make-transition "A" "S0") (make-transition "B" "S1"))) 
     20 20 false)
    (make-machine-state-gui
     (make-machine-state
      "S0" true false
      (list (make-transition "A" "S0") (make-transition "B" "S1"))) 
     20 20 false))
   (+ 20 INPUT-SYMBOL-X-POS-OFFSET))
  )

;;; machine-state-gui-transition-input-symbol-y :
;;;     MachineStateGUI MachineStateGUI -> Integer
;;; GIVEN : Two MachineStateGUIs that have a transition in between
;;; RETURNS : A y coordinate where the input symbol of that transition is drawn.
;;; EXAMPLES :
;;;   (machine-state-gui-transition-input-symbol-y
;;;    (make-machine-state-gui
;;;     (make-machine-state
;;;      "S0" true false
;;;      (list (make-transition "A" "S0") (make-transition "B" "S1"))) 
;;;     20 20 false)
;;;    (make-machine-state-gui
;;;     (make-machine-state
;;;      "S1" true false
;;;      (list (make-transition "B" "S1") (make-transition "C" "S2"))) 
;;;     40 40 false))
;;;   => (+ 20 14))
;;;   (machine-state-gui-transition-input-symbol-y
;;;    (make-machine-state-gui
;;;     (make-machine-state
;;;      "S0" true false
;;;      (list (make-transition "A" "S0") (make-transition "B" "S1"))) 
;;;     20 20 false)
;;;    (make-machine-state-gui
;;;     (make-machine-state
;;;      "S0" true false
;;;      (list (make-transition "A" "S0") (make-transition "B" "S1"))) 
;;;     20 20 false))
;;;   => (+ 20 INPUT-SYMBOL-Y-POS-OFFSET))
;;; STRATEGY : Use the template for MachineStateGUI

;;; FUNCTION DEFINITION :
(define (machine-state-gui-transition-input-symbol-y src dest)
  (+ (machine-state-gui-y src)
     (cond [(equal? src dest) INPUT-SYMBOL-Y-POS-OFFSET]
           [else
            (* (- (machine-state-gui-y dest)
                  (machine-state-gui-y src))
               INPUT-SYMBOL-Y-POS-WEIGHT)])))

;;; TESTS :
(begin-for-test
  (check-equal?
   (machine-state-gui-transition-input-symbol-y
    (make-machine-state-gui
     (make-machine-state
      "S0" true false
      (list (make-transition "A" "S0") (make-transition "B" "S1"))) 
     20 20 false)
    (make-machine-state-gui
     (make-machine-state
      "S1" true false
      (list (make-transition "B" "S1") (make-transition "C" "S2"))) 
     40 40 false))
   (+ 20 14))
  
  (check-equal?
   (machine-state-gui-transition-input-symbol-y
    (make-machine-state-gui
     (make-machine-state
      "S0" true false
      (list (make-transition "A" "S0") (make-transition "B" "S1"))) 
     20 20 false)
    (make-machine-state-gui
     (make-machine-state
      "S0" true false
      (list (make-transition "A" "S0") (make-transition "B" "S1"))) 
     20 20 false))
   (+ 20 INPUT-SYMBOL-Y-POS-OFFSET))
  )
;;; lomsg-state-gui-transition-input-symbol-images :
;;;     ListOfMachineStateGUI -> Image
;;; GIVEN : A list of MachineStateGUIs present in the current world
;;; RETURNS : A list of text images representing transitions of the machine
;;;     states of the given machine state guis.
;;; EXAMPLES :
;;;   (lomsg-state-gui-transition-input-symbol-images
;;;    (list (make-machine-state-gui
;;;           (make-machine-state
;;;            "S0" true false
;;;            (list (make-transition "A" "S0") (make-transition "B" "S1"))) 
;;;           20 20 false)
;;;          (make-machine-state-gui
;;;           (make-machine-state
;;;            "S1" true false
;;;            (list (make-transition "B" "S1") (make-transition "C" "S2"))) 
;;;           40 40 false)
;;;          (make-machine-state-gui
;;;           (make-machine-state
;;;            "S2" true false
;;;            (list (make-transition "A" "S0") )) 
;;;           60 60 false)))
;;;   => (list (text "A" INPUT-SYMBOL-FONT-SIZE INPUT-SYMBOL-FONT-COLOR)
;;;            (text "B" INPUT-SYMBOL-FONT-SIZE INPUT-SYMBOL-FONT-COLOR)
;;;            (text "B" INPUT-SYMBOL-FONT-SIZE INPUT-SYMBOL-FONT-COLOR)
;;;            (text "C" INPUT-SYMBOL-FONT-SIZE INPUT-SYMBOL-FONT-COLOR)
;;;            (text "A" INPUT-SYMBOL-FONT-SIZE INPUT-SYMBOL-FONT-COLOR)))
;;; STRATEGY : Use HOF fold for ListOfMachineStateGUI on lomsg

;;; FUNCTION DEFINITION :
(define (lomsg-state-gui-transition-input-symbol-images lomsg)
  (foldr
   ;;; MachineStateGUI ListOfImage -> ListOfImage
   ;;; GIVEN : A MachineStateGUI present in the current world.
   ;;; RETURNS : A list of text images representing transitions of the machine
   ;;;     state of the given machine state gui.
   (lambda (msg r)
     (append (machine-state-gui-transition-input-symbol-images msg) r))
   empty
   lomsg))

;;; TESTS :
(begin-for-test
  (check-equal?
   (lomsg-state-gui-transition-input-symbol-images
    (list (make-machine-state-gui
           (make-machine-state
            "S0" true false
            (list (make-transition "A" "S0") (make-transition "B" "S1"))) 
           20 20 false)
          (make-machine-state-gui
           (make-machine-state
            "S1" true false
            (list (make-transition "B" "S1") (make-transition "C" "S2"))) 
           40 40 false)
          (make-machine-state-gui
           (make-machine-state
            "S2" true false
            (list (make-transition "A" "S0") )) 
           60 60 false)))
   (list (text "A" INPUT-SYMBOL-FONT-SIZE INPUT-SYMBOL-FONT-COLOR)
         (text "B" INPUT-SYMBOL-FONT-SIZE INPUT-SYMBOL-FONT-COLOR)
         (text "B" INPUT-SYMBOL-FONT-SIZE INPUT-SYMBOL-FONT-COLOR)
         (text "C" INPUT-SYMBOL-FONT-SIZE INPUT-SYMBOL-FONT-COLOR)
         (text "A" INPUT-SYMBOL-FONT-SIZE INPUT-SYMBOL-FONT-COLOR)))
  )


;;; machine-state-gui-transition-input-symbol-images :
;;;     MachineStateGUI  -> ListOfImages
;;; GIVEN :  A MachineStateGUI in the current world state.
;;; RETURNS : A list of text iamges corresponding to the input symbols of the
;;;     given transitions.
;;; EXAMPLES :
;;;     (machine-state-gui-transition-input-symbol-images
;;;      (make-machine-state-gui
;;;       (make-machine-state
;;;        "S0" true false
;;;        (list (make-transition "A" "S0") (make-transition "B" "S1"))) 
;;;       20 20 false))
;;;     => (list (text "A" INPUT-SYMBOL-FONT-SIZE INPUT-SYMBOL-FONT-COLOR)
;;;              (text "B" INPUT-SYMBOL-FONT-SIZE INPUT-SYMBOL-FONT-COLOR))
;;;     (machine-state-gui-transition-input-symbol-images
;;;      (make-machine-state-gui
;;;       (make-machine-state "S0" true false empty) 20 20 false))
;;;     => empty)
;;; STRATEGY : Use HOF foldr for ListOfTransition
;;;      on (machine-state-gui-transitions msg)

;;; FUNCTION DEFINITION :
(define (machine-state-gui-transition-input-symbol-images msg)
  (foldr
   ;;; Transition ListOfImage -> ListOfImage
   ;;; GIVEN : A transition of a machine state 
   ;;; RETURNS : A list of text iamges corresponding to the input symbol of the
   ;;;     given transition.
   (lambda (t r)
     (cons (text (transition-label t)
                 INPUT-SYMBOL-FONT-SIZE INPUT-SYMBOL-FONT-COLOR) r))
   empty
   (machine-state-gui-transitions msg)))

;;; TESTS :
(begin-for-test
  (check-equal?
   (machine-state-gui-transition-input-symbol-images
    (make-machine-state-gui
     (make-machine-state
      "S0" true false
      (list (make-transition "A" "S0") (make-transition "B" "S1"))) 
     20 20 false))
   (list (text "A" INPUT-SYMBOL-FONT-SIZE INPUT-SYMBOL-FONT-COLOR)
         (text "B" INPUT-SYMBOL-FONT-SIZE INPUT-SYMBOL-FONT-COLOR)))
  
  (check-equal?
   (machine-state-gui-transition-input-symbol-images
    (make-machine-state-gui
     (make-machine-state "S0" true false empty) 20 20 false))
    empty)
  )

;;; lot-to-lomsg :
;;;     ListOfTransition ListOfMachineStateGUI -> ListOfMachineStateGUI
;;; GIVEN : A list of Transitions of a machine state and a list of
;;;     MachineStateGUIs present in the current world state.
;;; RETURNS : A list of MachineStateGUIs corresponding to the given transitions.
;;; EXAMPLES :
;;;     (lot-to-lomsg
;;;      (list (make-transition "A" "S0") (make-transition "B" "S1"))
;;;            (list (make-machine-state-gui
;;;             (make-machine-state
;;;              "S0" true false
;;;              (list (make-transition "A" "S0") (make-transition "B" "S1"))) 
;;;             20 20 false)
;;;            (make-machine-state-gui
;;;             (make-machine-state
;;;              "S1" true false
;;;              (list (make-transition "B" "S1") (make-transition "C" "S2"))) 
;;;             40 40 false)
;;;            (make-machine-state-gui
;;;             (make-machine-state
;;;              "S2" true false
;;;              (list (make-transition "A" "S0") )) 
;;;             60 60 false)))
;;;     => (list (make-machine-state-gui
;;;               (make-machine-state
;;;                "S0" true false
;;;               (list (make-transition "A" "S0") (make-transition "B" "S1"))) 
;;;               20 20 false)
;;;              (make-machine-state-gui
;;;               (make-machine-state
;;;               "S1" true false
;;;              (list (make-transition "B" "S1") (make-transition "C" "S2"))) 
;;;              40 40 false)))
;;; STRATEGY : Use HOF foldr for ListOfTransitions on lot

;;; FUNCTION DEFINITION :
(define (lot-to-lomsg lot lomsg)
  (foldr
   ;;; Transition ListOfMachineStateGUI -> ListOfMachineStateGUI
   ;;; GIVEN : A Transition of a machine state and a base list of
   ;;;     MachineStateGUIs 
   ;;; RETURNS : A list of MachineStateGUIs corresponding to the destnation
   ;;;     of the given Transition.
   (lambda (t r)
     (append (lomsg-by-name lomsg (transition-dest t)) r))
   empty
   lot))

;;; TESTS :
(begin-for-test
  (check-equal?
   (lot-to-lomsg
    (list (make-transition "A" "S0") (make-transition "B" "S1"))
    (list (make-machine-state-gui
           (make-machine-state
            "S0" true false
            (list (make-transition "A" "S0") (make-transition "B" "S1"))) 
           20 20 false)
          (make-machine-state-gui
           (make-machine-state
            "S1" true false
            (list (make-transition "B" "S1") (make-transition "C" "S2"))) 
           40 40 false)
          (make-machine-state-gui
           (make-machine-state
            "S2" true false
            (list (make-transition "A" "S0") )) 
           60 60 false)))
   (list (make-machine-state-gui
           (make-machine-state
            "S0" true false
            (list (make-transition "A" "S0") (make-transition "B" "S1"))) 
           20 20 false)
          (make-machine-state-gui
           (make-machine-state
            "S1" true false
            (list (make-transition "B" "S1") (make-transition "C" "S2"))) 
           40 40 false)))

  (check-equal?
   (lot-to-lomsg
    (list (make-transition "A" "S0") (make-transition "C" "S2"))
    (list (make-machine-state-gui
           (make-machine-state
            "S0" true false
            (list (make-transition "A" "S0") (make-transition "B" "S1"))) 
           20 20 false)
          (make-machine-state-gui
           (make-machine-state
            "S1" true false
            (list (make-transition "B" "S1") (make-transition "C" "S2"))) 
           40 40 false)
          (make-machine-state-gui
           (make-machine-state
            "S2" true false
            (list (make-transition "A" "S0") )) 
           60 60 false)))
   (list (make-machine-state-gui
          (make-machine-state
           "S0" true false
           (list (make-transition "A" "S0") (make-transition "B" "S1"))) 
          20 20 false)
         (make-machine-state-gui
          (make-machine-state
           "S2" true false
           (list (make-transition "A" "S0"))) 
          60 60 false)))

  )
   
    
;;; machine-state-gui-transitions : MachineStateGUI -> ListOfTransition
;;; GIVEN : A MachineStateGUI present in the current world state
;;; RETURNS : A list of Transitions of the machine state of the given machine
;;;      state gui.
;;; EXAMPLES :
;;;      (machine-state-gui-transitions
;;;         (make-machine-state-gui
;;;         (make-machine-state "S0" true false empty) 20 20 false))
;;;      => empty
;;;      (machine-state-gui-transitions
;;;         (make-machine-state
;;;         "S0" true false
;;;         (list (make-transition "A" "S0") (make-transition "B" "S1"))) 
;;;         20 20 false))
;;;      => (list (make-transition "A" "S0") (make-transition "B" "S1")))
;;; STRATEGY : Use the template for MachineStateGUI on msg

;;; FUNCTION DEFINITION :
(define (machine-state-gui-transitions msg)
  (machine-state-transitions (machine-state-gui-ms msg)))

;;; TESTS :
(begin-for-test
  (check-equal?
   (machine-state-gui-transitions
    (make-machine-state-gui
     (make-machine-state "S0" true false empty) 20 20 false))
   empty)
  
  (check-equal?
   (machine-state-gui-transitions
    (make-machine-state-gui
     (make-machine-state
      "S0" true false
      (list (make-transition "A" "S0") (make-transition "B" "S1"))) 
      20 20 false))
    (list (make-transition "A" "S0") (make-transition "B" "S1")))
  )
    
;;; lomsg-by-name : ListOfMachineStateGUI String -> ListOfMachineStateGUI
;;; GIVEN : A list of MachineStateGUIs present in the current world state and
;;;    the name of a machine state.
;;; RETURNS : A list of MachineStateGUIs corresponding to the given name.
;;; EXAMPLES :
;;;   (lomsg-by-name
;;;    (list (make-machine-state-gui
;;;           (make-machine-state
;;;            "S0" true false
;;;            (list (make-transition "A" "S0") (make-transition "B" "S1"))) 
;;;           20 20 false)
;;;          (make-machine-state-gui
;;;           (make-machine-state
;;;            "S1" true false
;;;            (list (make-transition "B" "S1") (make-transition "C" "S2"))) 
;;;           40 40 false)
;;;          (make-machine-state-gui
;;;           (make-machine-state
;;;            "S2" true false
;;;            (list (make-transition "A" "S0") )) 
;;;           60 60 false))
;;;    "S0")
;;;   => (list (make-machine-state-gui
;;;             (make-machine-state
;;;             "S0" true false
;;;             (list (make-transition "A" "S0") (make-transition "B" "S1"))) 
;;;             20 20 false))
;;; STRATEGY : Use HOF filter for ListOfMachineStateGUI on lomsg

;;; FUNCTION DEFINITION :
(define (lomsg-by-name lomsg statename)
   (filter
    ;;; MachineStateGUI -> Boolean
    ;;; GIVEN : A MachineStateGUI present in the current world state
    ;;; RETURNS : whether the name of the machine state of the given
    ;;;     MachineStateGUI is the same as the given name from lomsg-by-name.
    (lambda (msg)
      (string=? (machine-state-name (machine-state-gui-ms msg)) statename)) 
    lomsg))

;;; TESTS:
(begin-for-test
  (check-equal?
   (lomsg-by-name
    (list (make-machine-state-gui
           (make-machine-state
            "S0" true false
            (list (make-transition "A" "S0") (make-transition "B" "S1"))) 
           20 20 false)
          (make-machine-state-gui
           (make-machine-state
            "S1" true false
            (list (make-transition "B" "S1") (make-transition "C" "S2"))) 
           40 40 false)
          (make-machine-state-gui
           (make-machine-state
            "S2" true false
            (list (make-transition "A" "S0") )) 
           60 60 false))
    "S0")
   (list (make-machine-state-gui
          (make-machine-state
           "S0" true false
           (list (make-transition "A" "S0") (make-transition "B" "S1"))) 
          20 20 false)))
)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; For testing:
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; s0 is a start state but not an accepting state

(define s0
  (make-machine-state "S0" true false
                      (list (make-transition "b" "S1"))))

;;; s1 is an accepting state

(define s1
  (make-machine-state "S1" false true
                      (list (make-transition "a" "S0")
                            (make-transition "a" "S1")
                            (make-transition "b" "S2"))))

;;; s2 is neither a start state nor an accepting state

(define s2
  (make-machine-state "S2" false false
                      (list (make-transition "a" "S0"))))

(define nfa1 (list s1 s2 s0))

(define (make-n-states num)
  (foldr
   (lambda (n w) (create-states w n))
   INITIAL-WORLD
   (range 1 num 1)))

(define (create-states w n)
  (world-after-key-event
   (world-after-key-event
    (world-after-key-event w "N") (int->string n)) "\r"))

(define 2STATES (make-n-states 3))

(define 2S1T (world-after-mouse-event
 (world-after-mouse-event
  (world-after-mouse-event
   (world-after-mouse-event
    (world-after-key-event
     (world-after-key-event 2STATES "T") "a") 50 50 "button-down")
   50 50 "button-up") 150 50 "button-down") 150 50 "button-up"))

(define 2S2T (make-world
              (list
               (make-machine-state-gui
                (make-machine-state "\u0001" #false #false
                                    (list (make-transition "a" "\u0002")))
                150 50 #false)
               (make-machine-state-gui
                (make-machine-state
                 "\u0002"
                 #true
                 #false
                 (list (make-transition "b" "\u0001"))) 50 50 #false))
              "Normal" "" '() '() 2 (make-posn 150 50)))